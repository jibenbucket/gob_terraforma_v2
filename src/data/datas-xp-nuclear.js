const datas = {
  name: 'nuclear',
  maximumMoney: 2000,
  pathName: 'intro-nuclear',
  places : [
    {
      location: "Tchernobyl, Ukraine",
      name: "Tchernobyl, Ukraine",
      lat:  51.304339,
      lon:  30.207979,
      descr: "The Tchernobyl Nuclear disaster is widely considered to have been the worst power plant accident in history, and is one of only two classified as a level 7 event on the International Nuclear Event Scale (the other being the Fukushima, Daiichi disaster in 2011). The battle to contain the contamination and avert a greater catastrophe ultimately involved over 500,000 workers and cost an estimated 18 billion rubles.  The official Soviet casualty count of 31 deaths has been disputed and long-term effects such as cancers and deformities are still being accounted for.",
      date: "04.26.1986",
      image: "/images/nuclear/1.jpg",
      text: "Le texte"
    },
    {
      location: "Fukushima, Japan",
      lat: 37.768996,
      lon: 140.456512,
      name: "Fukushima catastrophe",
      descr: "The Fukushima Daiichi nuclear disaster (福島第一原子力発電所事故 Fukushima Dai-ichi genshiryoku hatsudensho jiko) was an energy accident at the Fukushima Daiichi Nuclear Power Plant in Ōkuma, Fukushima, initiated primarily by the tsunami following the Tōhoku earthquake on 11 March 2011. Immediately after the earthquake, the active reactors automatically shut down their sustained fission reactions. However, the tsunami disabled the emergency generators that would have provided power to control and operate the pumps necessary to cool the reactors. The insufficient cooling led to three nuclear meltdowns, hydrogen-air explosions, and the release of radioactive material in Units 1, 2, and 3 from 12 March to 15 March. Loss of cooling also caused the pool for storing spent fuel from Reactor 4 to overheat on 15 March due to the decay heat from the fuel rods.",
      date: "03.11.2011",
      image: "/images/nuclear/2.jpg",
      text: "Le texte"
    },
    {
      location: "Europe north Atlantic",
      lat: 45.839933,
      lon: -19.582709,
      name: "Europe north Atlantic waste dump zone",
      descr: "Nuclear waste dump zone by multiple countries such as UK, France, Switzerland, Belgium, Sweden, Germany and Italy.",
      date: "From 1948 to 1982",
      image: "/images/nuclear/waste1.jpg",
      text: "Le texte"
    },
    {
      location: "America north Atlantic",
      lat: 38.056377,
      lon: -73.542992,
      name: "America north Atlantic waste dump zone",
      descr: "Nuclear waste dump zone of USA.",
      date: "From 1948 to 1982",
      image: "/images/nuclear/waste2.jpg",
      text: "Le texte"
    },
    {
      location: "America north Pacific",
      lat: 45.920808,
      lon: -134.969270,
      name: "America north Pacific waste dump zone",
      descr: "Nuclear waste dump zone of USA.",
      date: "From 1948 to 1982",
      image: "/images/nuclear/waste3.jpg",
      text: "Le texte"
    },
    {
      location: "Japan sea",
      lat: 41.407523,
      lon: 132.430729,
      name: "Japan sea waste dump zone",
      descr: "Nuclear waste dump zone by multiple countries such as Japan, Soviet Union and South Korea.",
      date: "From 1948 to 1982",
      image: "/images/nuclear/waste4.jpg",
      text: "Le texte"
    },
    {
      location: "UK, Sellafield",
      lat: 54.416680,
      lon: -3.505615,
      name: "Windscale Fire",
      descr: "The worst nuclear disaster in Great Britain’s history occurred on the 10th October, 1957 and ranked at level 5 on the INES scale, The Windscale Fire. The two piles had been hurriedly built as part of the British atomic bomb project. The first pile was active from October 1950 with the second close behind in June 1951. The accident occurred when the core of Unit 1’s reactor caught fire, releasing substantial amounts of radioactive contamination into the surrounding area. 240 cancer cases have since been linked to the fire. All of the milk from within about 500km of nearby countryside was diluted and destroyed for about a month.",
      date: "10.10.57",
      image: "/images/nuclear/4.jpg",
      text: "Le texte"
    },
    {
      location: "Russia, Maïak nuclear complex",
      lat: 55.7125,
      lon: 60.84806,
      name: "Maïak nuclear complex explosion",
      descr: "Explosion of a nuclear waste reservoir, releasing a radioactive cloud contaminating a 800km2 area. 200 dead, 10 000 people are evacuated and more than 400 000 are exposed to radiations. It measured as a Level 6 disaster on the INES, making it the third most serious Nuclear disaster ever recorded behind the Chernobyl Disaster and Fukushima Daiichi Disaster. The event occurred in the town of Ozyorsk, a closed city built around the Mayak plant. Since Ozyorsk/Mayak was not marked on maps, the disaster was named after Kyshtym, the nearest known town.",
      date: "09.29.57",
      image: "/images/nuclear/3.jpg",
      text: "Le texte"
    },
    {
      location: "USA, Pennsylvania",
      lat: 40.152547,
      lon: -76.723913,
      name: "Three Mile Island Accident",
      descr: "28th March saw two nuclear reactors meltdown. It was subsequently the worst disaster in commercial nuclear power plant history. Small amounts of radioactive gases and radioactive iodine were released into the environment. Luckily, epidemiology studies have not linked a single cancer with the accident.",
      date: "03.28.79",
      image: "/images/nuclear/5.jpg",
      text: "Le texte"
    },
    {
      location: "Brazil, Goiana",
      lat: -16.707891,
      lon: -49.268657,
      name: "Goiana accident",
      descr: "On 13th September, 1987 a radioactive contamination accident occurred in the Brazilian state of Goais. An old radiotherapy source was stolen from an abandoned hospital site in the city. Subsequently it was handled by many people, killing four people. 112,000 people were examined for radioactive contamination’s with 249 having significant levels of radioactive material in or on their body.",
      date: "09.13.87",
      image: "/images/nuclear/6.jpg",
      text: "Le texte"
    },
    {
      location: "USA, Idaho",
      lat: 43.175628,
      lon: -112.659726,
      name: "Experimental Power Station",
      descr: "On 3rd January, 1961 a USA army experimental nuclear power reactor underwent a steam explosion and meltdown killing its three operators. The cause of this was because of improper removal of the control rod, responsible for absorbing neutrons in the reactor core. This event is the only known fatal reactor accident in the USA. The accident released about 80 curies of iodine -131.",
      date: "01.03.61",
      image: "/images/nuclear/7.jpg",
      text: "Le texte"
    },
    {
      location: "France, Saint Laurent des Eaux",
      lat: 47.719419,
      lon: 1.579867,
      name: "Saint Laurent",
      descr: "On the 17th October, 1969 50 kg of uranium in one of the gas cooled reactors began to melt. This was classified as Level 4 on the INES and to this day remains the most serious civil Nuclear disaster in French history.",
      date: "10.17.69",
      image: "/images/nuclear/8.jpg",
      text: "Le texte"
    },
    {
      location: "Argentina, Buenos Aires",
      lat: -34.627714,
      lon: -58.444172,
      name: "Buenos Aires",
      descr: "An operator’s errors during a fuel plate reconfiguration lead to him dying two days later. There was an excursion of 3×10 fissions at the RA-2 facility with the operator absorbing 2000 rad of gamma and 1700 rad of neutron radiation. Another 17 people outside of the reactor room absorbed doses ranging from 35 rad to less than 1 rad.",
      date: "1983",
      image: "/images/nuclear/9.jpg",
      text: "Le texte"
    }
  ],
  popUp : [
    {
      type: "zeroStep",
      text : "YOU NEED TO REACH THE FIRST STEP TO EARN MONEY AND BUY RESOURCES TO RENATURALIZE YOUR ENVIRONEMENT. ARE YOU REALLY SURE TO QUIT THE MISSION ?",
      btnConfirmTxt : "QUIT MISSION",
      btnCancelTxt : "CONTINUE"
    },
    {
      type: "firstStep",
      title: "YOU'VE ACHIEVED THE FIRST STEP",
      text: "YOU CAN NOW QUIT THE MISSION AND BUY RESSOURCES TO RENATURALIZE. OR YOU CAN CONTINUE THE MISSION TO EARN MORE MONEY AND PRESS 'BACK TO HOME' LATER.",
      btnConfirmTxt : "BUY RESOURCES",
      btnCancelTxt : "DISCOVER MORE"
    },
    {
      type: "halfStep",
      title: "YOU'VE ACHIEVED THE HALF",
      text: "YOU CAN NOW QUIT THE MISSION AND BUY RESSOURCES TO RENATURALIZE. OR YOU CAN CONTINUE THE MISSION TO EARN MORE MONEY AND PRESS 'BACK TO HOME' LATER.",
      btnConfirmTxt : "BUY RESOURCES",
      btnCancelTxt : "DISCOVER MORE"
    },
    {
      type: "missionDone",
      title: "MISSION ACCOMPLISHED",
      text: "CONGRATS, YOU FINISHED THE MISSION BY DISCOVERING EVERYTHING. YOU CAN NOW BUY RESSOURCES TO RENATURALIZE.",
      btnConfirmTxt : "BUY RESOURCES"
    }
  ]
};
export default datas;
