const datas = {
  name: 'pesticides',
  maximumMoney: 1000,
  pathName: 'intro-pesticides',
  seedStartSeconds : 1000,
  levels : [
    {
      z: 0,
      objects :
      [
        {
          name: "bee",
          color: 0xffcc00,
          angle: 10,
          descr: "Pesticides may kill bees and are heavily involved in pollinator decline, the loss of species that pollinate plants, including the collapse disorder bee colonies in which worker bees from a beehive or a European bee colony abruptly disappear. The application of pesticides when crops are flowering can kill honeybees, which act as pollinators.",
          image: "/images/pesticide/bee.jpg"
        },
  //       {
  //         name: "bird",
  //         color: 0x87244C,
  //         angle: -120,
  //         descr: "The US Department of Fisheries and Wildlife estimates that each year 72 million birds are killed by pesticides in the United States.\
  // Throughout Europe, 116 bird species have been endangered from 1999. It was found that the decline of bird populations is associated with periods and pesticide use areas. The thinning of the egg shell induced DDE particularly affected the populations of European and North American birds.",
  //         image: "/images/pesticide/bird.jpg"
  //       },
        {
          name: "frog",
          color: 0x8fbf78,
          angle: 200,
          descr: "In recent decades, amphibian populations have declined worldwide, for unexplained reasons, probably different, but in which the pesticides may be partially involved.\
  Pesticide mixtures seem to have a cumulative toxic effect on frogs. The tadpoles living in polluted ponds by multiple pesticides take longer to metamorphose and are smaller, reducing their ability to capture prey and avoid predators.",
          image: "/images/pesticide/frog.jpg"

        }
      ]
    },
    {
      z: 400,
      objects :
      [
        {
          name: "fish",
          color: 0xa7dbd8,
          angle: -10,
          descr: "Fish and other aquatic organisms can be affected by water contaminated by pesticides. Pesticide runoff in streams can be highly lethal to aquatic life, sometimes killing all the fish of a particular stream.",
          image: "/images/pesticide/fish.jpg"
        },
        {
          name: "human",
          color: 0xfae6c5,
          angle: 160,
          descr: "Pesticides can enter the body by inhalation of aerosols, dust and fumes that contain pesticides, orally by consuming food or water, and by direct contact with the skin. Pesticides seep into the soil and groundwater, which may end up in drinking water, and pesticide sprays may drift and pollute the air. The effects of pesticides on human health depend on the toxicity of the chemical and the duration and extent of exposure. Agricultural workers and their families suffer the greatest exposure to agricultural pesticides by direct contact. Every human being contains traces of pesticides in their fat cells.",
          image: "/images/pesticide/human.jpg"
        }
      ]
    },
    {
      z: 800,
      objects :
      [
        {
          name: "air",
          color: 0xebf2f2,
          angle: 10,
          descr: "Pesticides can contribute to air pollution. Pesticide drift occurs when pesticides suspended in the air as particles are carried by wind to other areas, potentially contaminating them.\
  Pesticides that are sprayed on the fields and used for soil fumigation can release chemicals called volatile organic compounds, which can react with other chemicals to form a pollutant called tropospheric ozone. The use of pesticides is about six percent of total tropospheric ozone levels.",
          image: "/images/pesticide/air.jpg"
        },
        {
          name: "water",
          color: 0x69d2e7,
          angle: -30,
          descr: "In the US, it was found that pesticides pollute every stream and over 90% of wells sampled in a study by the US Geological Survey. Pesticide residues were also found in the rain and groundwater. Studies by the Government of the United Kingdom showed that pesticide levels exceeded the permissible values ​​for drinking water in some water samples from river and groundwater.",
          image: "/images/pesticide/water.jpg"
        },
        {
          name: "earth",
          color: 0xb4a387,
          angle: 200,
          descr: "Soil pollution is the accumulation of toxic compounds chemicals, salts, radioactive materials or pathogens, all of which have adverse effects on plant growth and health of animals. The increased use of fertilizer and many insecticides and fungicides after the end of the Second World War, from the 1960s, of concern on soil conditions. Certainly, the application of fertilizers containing major nutrients, namely nitrogen, phosphorus and potassium, has not, overall, contributed to soil pollution; however, excessive inputs of nitrogen fertilizer and phosphorus contribute to water pollution; Similarly, excessive use of trace elements can harm the soil condition. The irrigation of arid lands often leads to contamination by salt.",
          image: "/images/pesticide/earth.jpg"
        }
      ]
    },
     {
      z: 1200,
      objects :
      [
        {
          name: "politic",
          color: 0x547980,
          angle: 10,
          descr: "The European Food Safety Authority (EFSA) and the World Health Organization (WHO) are infiltrated by the pesticide lobby. This is the conclusion of a well-documented report, published February 4, 2014, by Pan-Europe, a European network of Pesticide Action.\
          The NGO estimates that 52% of EFSA's experts, 73% of the WHO, working on the effects of mixtures of pesticides in food, have links of varying strength - single adviser, former employee, or even, for some, former leader - with the chemical industry and agribusiness. These disorders links between private interests and independent expertise would explain the nine-year delay in the implementation of the 2005 EU directive that aims to protect consumers against pesticides. Indeed, EFSA has not released the cumulative risk assessment methodologies for pesticide mixtures.",
          image: "/images/pesticide/politic.jpg"
        },
        {
          name: "economic",
          color: 0x2fed65,
          angle: -35,
          descr: "Worldwide, about 3 billion kilograms of pesticides are applied annually. In the US, 500 million kg more than 600 different types of pesticides are used annually on crops. Despite the widespread application of pesticides at recommended doses, pests (insects, plant pathogens and weeds) destroy the United States provided 37% of crops. Insects destroy 13% of pathogens in plants 12% and 12% weeds. But on average, every dollar invested in pesticides worth about $ 4 products of higher crop production and \"protected\".\
  The main economic and environmental losses due to the application of pesticides in the US are: public health: 1.1 billion per year; pesticide resistance in pests 1.5 billion dollars annually; crop losses caused by pesticides: 1.4 billion dollars annually; bird losses due to pesticides 2.2 billion annually; and contamination of groundwater 2 billion dollars annually.\
  The US study concluded in 2005 that the benefit / cost ratio was slightly negative if there is added the hidden economic costs.",
          image: "/images/pesticide/economic.jpg"
        }
      ]
    }
  ]
};
export default datas;
