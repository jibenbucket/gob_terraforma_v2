import Application from 'containers/Application';
import ga from 'vue-ga'

import Router from 'core/Router';

import 'stylesheets/main.scss';

import domready from 'domready';

import 'three/examples/js/GPUParticleSystem.js';

import 'gsap';

class Main {

  constructor() {

    this.bind();

    this.addEventListeners();

    this.router = Router;

    ga(this.router, 'UA-110225010-1')

    this.start();
  }

  bind() {}

  addEventListeners() {}

  start() {

    this.router.start(Application, '#application');

  }
}

domready(() => {

  new Main();
});
