import THREE from 'three';

import Video from './objects/Video';

export default class Webgl {

  constructor(videoPath) {

    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.videoPath = videoPath;

    this.scene = new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1100 );
    this.camera.target = new THREE.Vector3( 0, 0, 0 );

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setPixelRatio( window.devicePixelRatio );
    // this.renderer.setSize( window.innerWidth, window.innerHeight );
    // this.renderer.setSize(width, height);
    this.videoEl = new Video(this.width, this.height, 500, this.videoPath);
    this.scene.add(this.videoEl);

  }

  resize(width, height) {
    this.width = width;
    this.height = height;
    if (this.composer) {
      this.composer.setSize(width, height);
    }

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
  }

  render() {
      this.renderer.setSize(this.width, this.height);
      this.renderer.render(this.scene, this.camera);
  }
}
