import THREE from 'three';
window.THREE = THREE;
import ParticlesBackground from './objects/ParticlesBackground';
import Emitter from 'core/Emitter';
import {
  SHOW_INFOS_POP
} from 'config/messages';
import Light from './objects/Light';
import Level from './objects/Level';
import Plant from './objects/Plant';
import Video from './objects/Video';
import TweenMax from 'gsap';

//const
const nbLevels = 4;
const cursorLightRadius = 20;
const lightRadius = 60;

const cameraOffset = 200;
const objectCameraOffset = 50;
const lightOffset = 1;

const distanceStep = 50;//Math.pow(lightOffset*1.20, 2);;

// DEGUEU ! MIEUX VAUT UTILISER CENTROID
const objH = 0;
const counterStep = 50;

export default class XpPesticidesWebgl {

  constructor(width, height) {
    this.params = {
      x: 0//,   y: 1,     z: 7
    };

    this.scene = new THREE.Scene();
    this.clock = new THREE.Clock();
    this.light = new THREE.AmbientLight( 0xffffff , 0.5); // soft white light //was 0.15
    this.scene.add( this.light );

    // CURSOR
    this.light = new Light(1);
    this.scene.add(this.light);

    this.camera = new THREE.PerspectiveCamera(45, width / height, 1, 5000);
    this.camera.position.set(0,0,cameraOffset);
    this.camera.target = new THREE.Vector3( 0, 0, 0 );
    this.camera.lookAt(this.camera.target);

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(width, height);
    this.renderer.setClearColor(0x000000);
    this.renderer.autoClear = false;

    // Flower offset
    let pointLight1 = new THREE.PointLight( 0x000000, 2, cursorLightRadius ); //PointLight( color, intensity, distance, decay )
    pointLight1.position.set(0,0,5);
    this.scene.add( pointLight1 );

    this.background = new ParticlesBackground(400);
    this.scene.add(this.background);

    this.progress = 0;
    this.levels = [];
    for(let i = 0; i < nbLevels; i++) {
      let level = new Level(i);
      level.loadLevelWithPoints(this.scene,cameraOffset,this.camera.fov,this.camera.aspect);
      this.levels.push(level);
    }
    this.levelIndex = 0;
    this.nbObjectsFound = new Array(this.levels.length);
    for(let i = 0; i < this.nbObjectsFound.length; i++) {
      this.nbObjectsFound[i] = 0;
    }

    this.plant = new Plant();
    this.scene.add(this.plant);
    let spotLight = new THREE.SpotLight( 0xffffff, 1.5, 60, 0.8, 0, 1);// new THREE.Spotlight(hex, intensity, distance, angle, exponent, decay)
    spotLight.position.set(0,0,30);
    spotLight.target.position.set(0,0,0);
    spotLight.target.updateMatrixWorld();
    let spotLightHelper = new THREE.SpotLightHelper(spotLight);
    this.scene.add(spotLight);

    this.isLeveling = false;
    this.isShowingInfo = false;
    this.isReady = false;
    this.isRendering = true;
  }

  getMax() {
    let max = 0;
    for(let i = 0; i < nbLevels; i++) {
      max += this.levels[i].nbObjects;
    }
    return max;
  }

  getNbObjectsLoaded() {
    let nb = 0;
    for(let i = 0; i < nbLevels; i++) {
      nb += this.levels[i].objects.length;
    }
    return nb;
  }

  setProgressFromLocalStorage() {
    this.infosIndexes = JSON.parse(localStorage.getItem('xpPesticidesInfosIndexes'));
    if(this.infosIndexes){
      for(let i=0; i < this.infosIndexes.length; i++){
        for(let j=0; j < this.infosIndexes[i].length; j++){
          // let levelIndex = this.infosIndexes[i];
          let levelIndex = i;
          let objectIndex = parseInt(this.infosIndexes[i][j]);
          this.nbObjectsFound[levelIndex] ++;
          this.progress ++;
          this.levels[levelIndex].objects[objectIndex].isFound = true;
          this.levels[levelIndex].objects[objectIndex].counter = counterStep;
          this.levels[levelIndex].objects[objectIndex].mesh.material.opacity = 0.8;
        }
      }
      let i=0;
      let stopLoop = false;
      for(;!stopLoop && i < this.levels.length; i++) {
        for(let j=0;!stopLoop && j < this.levels[i].objects.length; j++) {
          stopLoop = !this.levels[i].objects[j].isFound
          if(this.levels[i].objects[j].isFound == null) {
            stopLoop = true;
          }
        }
      }
      if(i > 1)
        this.levelIndex = i-1;

      this.levelUpTo(this.levelIndex,2);

    } else {
      this.infosIndexes = new Array(this.levels.length);
      for(let i=0; i < this.levels.length; i++){
        this.infosIndexes[i] = [];
      }
      this.progress = 0;
    }
  }

  saveProgresToLocalStorage(){
    localStorage.setItem('xpPesticidesInfosIndexes',JSON.stringify(this.infosIndexes));
  }

  resize(width, height) {
    this.width = width;
    this.height = height;

    if (this.composer) {
      this.composer.setSize(width, height);
    }

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
  }

  render() {
    if(this.isRendering) {
      this.update();
      this.renderer.render(this.scene, this.camera);
    }
  }

  resumeRender() {
    this.isRendering = true;
  }

  pauseRender() {
    this.isRendering = false;
  }

  update() {
    let delta = 10 * this.clock.getDelta();
    if (this.isReady) {
      if (this.plant.hasRised == false) {
        this.plant.update(delta);
        for (let i = 0; i < this.levels.length; i++) {
          this.levels[i].updateOnlyFound(this.light.positionTarget.clone(),delta);
        }
      } else {
        this.light.updateParticles(delta);
        this.onMouseMove();
        this.levels[this.levelIndex].update(this.light.positionTarget.clone(),delta);
        if(!this.isShowingInfo) {
          for(let i = 0; i < this.levels[this.levelIndex].objects.length; i ++) {
            if(!this.levels[this.levelIndex].objects[i].isFound) {
              let xd = this.levels[this.levelIndex].objects[i].mesh.position.x - this.light.mesh.position.x;
              let yd = this.levels[this.levelIndex].objects[i].mesh.position.y - this.light.mesh.position.y;
              let zd = this.levels[this.levelIndex].objects[i].mesh.position.z - this.light.mesh.position.z;
              let distance = Math.abs( xd ) + Math.abs( yd ) + Math.abs( zd );
              //let distance = xd*xd + yd*yd + zd*zd;
              if(distance < distanceStep) {
                    //on incrémente jusqu'à une certaine valeur qui affiche ensuite
                    this.levels[this.levelIndex].objects[i].counter ++;
                    //this.objectPointLight.distance += 0.01;
                    if(this.levels[this.levelIndex].objects[i].counter > counterStep) {
                      this.showInfo(i);
                    }
              } else {
                    this.levels[this.levelIndex].objects[i].counter = 0;
              }
            }
          }
        }
      }
    }
  }

  showInfo(index) {
    // this.progress ++;
    this.selectedObjectIndex = index;
    this.infosIndexes[this.levelIndex].push(index);
    this.progress ++;

    this.nbObjectsFound[this.levelIndex] ++;
    this.levels[this.levelIndex].objects[index].isFound = true;
    this.levels[this.levelIndex].objects[index].mesh.material.opacity = 0.8;

    let spotLight = new THREE.SpotLight( 0xffffff, 1.5, lightRadius, 0.5, 0, 1);// new THREE.Spotlight(hex, intensity, distance, angle, exponent, decay)
    spotLight.position.copy(this.levels[this.levelIndex].objects[index].mesh.position);
    spotLight.position.z += 30;
    spotLight.target.position.copy(this.levels[this.levelIndex].objects[index].mesh.position);
    spotLight.target.updateMatrixWorld();
    let spotLightHelper = new THREE.SpotLightHelper(spotLight);
    this.scene.add(spotLight);
    this.saveProgresToLocalStorage();
    // animation material
    // this.levels[this.levelIndex].objects[index].material = this.levels[this.levelIndex].objects[index].mate;
    // for (var i = 0; i < this.levels[this.levelIndex].objects[index].material.materials.length; i++) {
    //   this.levels[this.levelIndex].objects[index].material.materials[i].opacity = 0;
    //   this.levels[this.levelIndex].objects[index].material.materials[i].transparent = true;
    //   TweenMax.to(this.levels[this.levelIndex].objects[index].material.materials[i],2,{opacity:1});
    // }

    // animation camera
    this.camera.target = this.levels[this.levelIndex].objects[index].mesh.position;
    this.camera.lookAt(this.camera.target);
    this.camera.updateProjectionMatrix();
    TweenMax.to(this.camera.position,1,{x:this.levels[this.levelIndex].objects[index].mesh.position.x});
    TweenMax.to(this.camera.position,1,{y:this.levels[this.levelIndex].objects[index].mesh.position.y});
    TweenMax.to(this.camera.position,1,{z:this.levels[this.levelIndex].objects[index].mesh.position.z+objectCameraOffset,
      onUpdate:()=>{
                        this.camera.target = this.levels[this.levelIndex].objects[index].mesh.position;
                        this.camera.lookAt(this.camera.target);
                        this.camera.updateProjectionMatrix();
                        this.light.hide();
                    }, onComplete:() => {
                      this.isShowingInfo = true;
                      Emitter.emit(SHOW_INFOS_POP);
                    }
                  });
  }

  resetCameraPosition() {
    TweenMax.to(this.camera.position,0.5,{x:0});
    TweenMax.to(this.camera.position,0.5,{y:0});
    TweenMax.to(this.camera.position,0.5,{z:this.levels[this.levelIndex].zPosition+cameraOffset,onUpdate:()=>{
                        this.camera.updateProjectionMatrix();
                    },onComplete:()=>{
                      this.camera.target = new THREE.Vector3( 0, 0, 0 );
                      this.camera.lookAt(this.camera.target);
                      this.camera.updateProjectionMatrix();
                      this.isShowingInfo = false;
                      this.light.show();
                      if(this.nbObjectsFound[this.levelIndex] == this.levels[this.levelIndex].nbObjects) {
                        this.levelUp(1);
                    }}
                    });
  }

  levelUp(duration) {
    if(this.levelIndex < nbLevels-1 && !this.isLeveling) {
      this.isLeveling = true;
      this.levelIndex ++;
      TweenMax.to(this.camera.position,duration,{z:(this.levels[this.levelIndex].zPosition + cameraOffset),onUpdate:()=>{
                          this.background.position.z = this.camera.position.z - cameraOffset;
                          this.camera.updateProjectionMatrix();
                          this.onMouseMove();
                      }, onComplete:()=>{this.isLeveling = false;}});
    }
  }

  levelUpTo(levelIndex, duration) {
    TweenMax.to(this.camera.position,duration,{z:(this.levels[this.levelIndex].zPosition + cameraOffset),onUpdate:()=>{
                        this.background.position.z = this.camera.position.z - cameraOffset;
                        this.camera.updateProjectionMatrix();
                        this.onMouseMove();
                    }, onComplete:()=>{this.isLeveling = false;}});
  }

  levelDown(duration) {
    if(this.levelIndex > 0 && !this.isLeveling) {
      this.isLeveling = true;
      this.levelIndex --;
      TweenMax.to(this.camera.position,duration,{z:(this.levels[this.levelIndex].zPosition + cameraOffset),onUpdate:()=>{
                          this.background.position.z = this.camera.position.z - cameraOffset;
                          this.camera.updateProjectionMatrix();
                          this.onMouseMove();
                      }, onComplete:()=>{this.isLeveling = false;}});
    }
  }

  onClick(event) {
    // this.levels[this.levelIndex].moveTo(this.light.positionTarget.clone());
  }

  onMouseMove(event) {
    // Mouse following
    if(!this.isShowingInfo) {
      if (typeof event !== 'undefined') {
        event.preventDefault();
        this.mouseX = (event.clientX / window.innerWidth) * 2 - 1;
        this.mouseY = - (event.clientY / window.innerHeight) * 2 + 1;
      }
      // this.camera.position.x += ( this.mouseX - this.camera.position.x ) * .05;
			// this.camera.position.y += ( - this.mouseY + 200 - this.camera.position.y ) * .05;
			// this.camera.lookAt( this.camera.target );

      let vector = new THREE.Vector3(this.mouseX, this.mouseY, lightOffset*2 + this.levels[this.levelIndex].zPosition);
      vector.unproject(this.camera);
      let dir = vector.sub( this.camera.position ).normalize();
      let pos = this.camera.position.clone().add( dir.multiplyScalar( - cameraOffset ) );
      let prevPos = this.light.positionTarget.clone();
      pos.sub(prevPos);
      pos.multiplyScalar(0.1);
      prevPos.add(pos);
      this.light.moveTo(prevPos);

      // particles of the light
      let previousPos = this.light.positionTarget.clone();
      let d = Math.abs( previousPos.x - prevPos.x ) + Math.abs( previousPos.y - prevPos.y ) + Math.abs( previousPos.z - prevPos.z );
      this.light.spawnerOptions.spawnRate = 1000*(1-d);
    }
  }

  onkeydown(e) {
    if(!this.isShowingInfo) {
      e = e || window.event;
      if (e.keyCode == '38') {
          this.levelUp();
      }
      else if (e.keyCode == '40') {
          this.levelDown();
      }
    }
  }

}
