import THREE from 'three';
import d from 'data/datas-xp-pesticides';
import { upgradeObject } from '../../../utils/UpgradedObject';
import { upgradeObjectWithMultiplier } from '../../../utils/UpgradedObject';
import { upgradeObjectWithMultiplierAndMate } from '../../../utils/UpgradedObject';
import { upgradeObjectWithFoundAndCounter } from '../../../utils/UpgradedObject';
const radius = 10;
const counterStep = 50;

export default class Level {
  constructor(index) {
    this.index = index;
    this.objects = new Array(d['levels'][this.index]["objects"].length);
    this.nbObjects = d['levels'][this.index]["objects"].length;
    this.zPosition = d['levels'][this.index]["z"];
    this.visible = false;
    this.objectsFoundIndexes = new Array(this.nbObjects);
    for (let i = 0; i < this.objectsFoundIndexes.length; i++) {
      this.objectsFoundIndexes[i] = false;
    }
    this.clonemeshes = [];
  }

  loadLevelWithPoints(scene,cameraOffset,fov,aspect) {
    let objLoader = new THREE.JSONLoader();
    // let textureLoader = new THREE.TextureLoader();
    // let texture = textureLoader.load( "textures/objects/frog.png" );
    let path;

    let height = Math.tan( fov* Math.PI / 180 / 2 ) * cameraOffset;
    let width = height * aspect;
    let radius = width*3/4;
    let angle;
    let name;
    for(let i = 0; i < d['levels'][this.index]["objects"].length; i++) {
      name = d['levels'][this.index]["objects"][i]["name"];
      path = "./objects/js/"+name;
			objLoader.load(path+".js", ( geometry, materials ) => {
          angle = d['levels'][this.index]["objects"][i]["angle"]*Math.PI/180;
          let vl = geometry.vertices.length;

          let sphereGeometry = new THREE.Geometry();
          let vertices_tmp = [];
          let originalVertices;
          let r = 5;

          for ( let j = 0; j < vl; j ++ ) {
            let theta = Math.random()*Math.PI*2;
            let phi = Math.random()*Math.PI*2;

            sphereGeometry.vertices[ j ] = new THREE.Vector3(r * Math.sin(theta) * Math.cos(phi),
                                            r * Math.sin(theta) * Math.sin(phi),
                                            r * Math.cos(theta) );
            vertices_tmp[ j ] = [ geometry.vertices[ j ].x, geometry.vertices[ j ].y, geometry.vertices[ j ].z, 0, 0 ];

          }
          originalVertices = sphereGeometry.vertices;
          let material = new THREE.PointsMaterial( { size: 0.35, color: d['levels'][this.index]["objects"][i]["color"], transparent:true });
          // let material = new THREE.MeshLambertMaterial( { color: 0xa467b1*Math.random(), diffuse: 0xff0000  , side : THREE.DoubleSide});
          let mesh = new THREE.Points( sphereGeometry, material);
          // let mesh = new THREE.Mesh( geometry, faceMaterial );
          mesh.position.set(radius * Math.cos(angle),
                              radius * Math.sin(angle),
                              d['levels'][this.index]["z"]);
          // console.log(mesh.name);

          // mesh.name = d['levels'][this.index]["objects"][i]["name"];
          mesh.name = name;
          mesh.scale.x = 2+1*(this.index/d['levels'].length); mesh.scale.y = 2+1*(this.index/d['levels'].length); mesh.scale.z = 2+1*(this.index/d['levels'].length);
          mesh.lookAt(new THREE.Vector3(0,0,mesh.position.z + cameraOffset));
          mesh.rotateOnAxis(new THREE.Vector3(0,1,0),-90*Math.PI/180);
          mesh.material.opacity = 0;

          this.clonemeshes.push( { mesh: mesh, speed: 8 + Math.random() } );

          this.objects[i] = {
            mesh: mesh, vertices: sphereGeometry.vertices, original_vertices:originalVertices, vertices_tmp: vertices_tmp, vl: vl, direction: 0, speed: 2,
            dynamic: true, name: name//, name: d['levels'][this.index]["objects"][i]["name"]
          };
          scene.add(mesh);
			});
    }
  }

  update(lightPosition, delta) {
    delta = delta < 2 ? delta : 2;
    for (let objectIndex = 0; objectIndex < this.objects.length; objectIndex++) {
      let data = this.objects[objectIndex];
      let mesh = data.mesh;
      let targetVertices;
      let vl = data.vertices.length;

      if(this.objects[objectIndex].counter > 0) {
        targetVertices = data.vertices_tmp;
      } else {
        let dist = Math.abs( mesh.position.x - lightPosition.x ) + Math.abs( mesh.position.y - lightPosition.y ) + Math.abs( mesh.position.z - lightPosition.z );
        mesh.material.opacity = 0.5-dist/2.5/100;
        targetVertices = data.original_vertices;
        // mesh.material.color.setHSL( 0.3-dist/3/100, 0.3-dist/3/100, 0.3-dist/3/100 );
      }

      for ( let vIndex = 0; vIndex < vl; vIndex ++ ) {
        let vt = targetVertices[vIndex];

        let p = data.vertices[vIndex];

          let d = Math.abs( p.x - vt[0] ) + Math.abs( p.y - vt[1] ) + Math.abs( p.z - vt[2] );
          if ( d > 0.1 ) {
            p.x += - ( p.x - vt[0] ) / d * data.speed * delta * ( 0.85 - Math.random() );
            p.y += - ( p.y - vt[1] ) / d * data.speed * delta * ( 0.85 - Math.random() );
            p.z += - ( p.z - vt[2] ) / d * data.speed * delta * ( 0.85 - Math.random() );

            // p.x -= 1.5 * ( 0.50 - Math.random() ) * data.speed ;
            // p.y -= 3.0 * ( 0.25 - Math.random() ) * data.speed ;
            // p.z -= 1.5 * ( 0.50 - Math.random() ) * data.speed ;
          } else {
            if ( ! vt[ 4 ] ) {
              vt[ 4 ] = 1;
              data.up += 1;
             }
          }
        }
      mesh.geometry.verticesNeedUpdate = true;
    }
  }

  updateOnlyFound(lightPosition, delta) {
    delta = delta < 2 ? delta : 2;
    for (let objectIndex = 0; objectIndex < this.objects.length; objectIndex++) {
      if(this.objects[objectIndex].isFound){
        let data = this.objects[objectIndex];
        let mesh = data.mesh;
        let targetVertices;
        let vl = data.vertices.length;

        if(this.objects[objectIndex].counter > 0) {
          targetVertices = data.vertices_tmp;
        } else {
          let dist = Math.abs( mesh.position.x - lightPosition.x ) + Math.abs( mesh.position.y - lightPosition.y ) + Math.abs( mesh.position.z - lightPosition.z );
          // mesh.material.opacity = 0.3-dist/3/100;
          targetVertices = data.original_vertices;
        }

        for ( let vIndex = 0; vIndex < vl; vIndex ++ ) {
          let vt = targetVertices[vIndex];

          let p = data.vertices[vIndex];

          let d = Math.abs( p.x - vt[0] ) + Math.abs( p.y - vt[1] ) + Math.abs( p.z - vt[2] );
          if ( d > 0.1 ) {
            p.x += - ( p.x - vt[0] ) / d * data.speed * delta * ( 0.85 - Math.random() );
            p.y += - ( p.y - vt[1] ) / d * data.speed * delta * ( 0.85 - Math.random() );
            p.z += - ( p.z - vt[2] ) / d * data.speed * delta * ( 0.85 - Math.random() );

            // p.x -= 1.5 * ( 0.50 - Math.random() ) * data.speed ;
            // p.y -= 3.0 * ( 0.25 - Math.random() ) * data.speed ;
            // p.z -= 1.5 * ( 0.50 - Math.random() ) * data.speed ;
          } else {
            if ( ! vt[ 4 ] ) {
              vt[ 4 ] = 1;
              data.up += 1;
            }
          }
        }
        mesh.geometry.verticesNeedUpdate = true;
      }
    }
  }

  moveTo(position) {
    for (let objectIndex = 0; objectIndex < this.objects.length; objectIndex++) {
      let data = this.objects[objectIndex];
      let vl = data.vertices.length;
      data.target = position;
    }
  }

}
