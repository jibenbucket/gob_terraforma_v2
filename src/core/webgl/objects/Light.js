
import THREE from 'three';
import dat from 'dat-gui';

let gui;

export default class Light extends THREE.Object3D {
  constructor(radius,color) {
    super();
    this.radius = radius;
    this.color = (color !== undefined) ? color : 0xffffff;
    this.geom = new THREE.SphereGeometry(this.radius , 32, 32 );
// new THREE.MeshLambertMaterial( { color: 0x666666, emissive: 0xff0000, shading: THREE.SmoothShading } )

    this.mat =
    //new THREE.MeshLambertMaterial( { color: 0x666666, emissive: 0xff0000, shading: THREE.SmoothShading } )
     new THREE.MeshBasicMaterial( { color: 0xffffff, transparent: true, blending: THREE.AdditiveBlending, wireframe:true } )
    // new THREE.MeshNormalMaterial({
    //   shading: THREE.SmoothShading,
    //   color: this.color,
    //   wireframe: false
    // })
    ;
    this.mesh = new THREE.Mesh(this.geom, this.mat);
    this.mesh.name = "light";

    // this.add(this.mesh);

    this.pointLight = new THREE.PointLight( 0xffffff, 1, 50 ); //PointLight( color, intensity, distance, decay )
    this.add( this.pointLight );

    this.clock = new THREE.Clock(true);
    this.tick = 0;

    this.createParticles();
    this.positionTarget = new THREE.Vector3();

    // this.addGui();

  }

  createParticles() {
    // The GPU Particle system extends THREE.Object3D, and so you can use it
			// as you would any other scene graph component.	Particle positions will be
			// relative to the position of the particle system, but you will probably only need one
			// system for your whole scene
			this.particleSystem = new THREE.GPUParticleSystem({
				maxParticles: 250000
			});
			this.add( this.particleSystem);
			// options passed during each spawned
			this.options = {
				position: new THREE.Vector3(),
				positionRandomness: 8,
				velocity: new THREE.Vector3(),
				velocityRandomness: 3,
				color: 0x666666,
				colorRandomness: .02,
				turbulence: 0,
				lifetime: 4,
				size: 7,
				sizeRandomness: 1
			};
			this.spawnerOptions = {
				spawnRate: 10000,
				horizontalSpeed: 1,
				verticalSpeed: 1,
				timeScale: 0.08
			};

      // this.addGui();
  }

  addGui() {
    gui = new dat.GUI();
    gui.add(this.options, "velocityRandomness", 0, 20);
    gui.add(this.options, "positionRandomness", 0, 20);
    gui.add(this.options, "size", 1, 20);
    gui.add(this.options, "sizeRandomness", 0, 25);
    gui.add(this.options, "colorRandomness", 0, 1);
    gui.add(this.options, "lifetime", .1, 10);
    gui.add(this.options, "turbulence", 0, 5);
    gui.add(this.spawnerOptions, "spawnRate", 10, 30000);
    gui.add(this.spawnerOptions, "timeScale", -1, 1);
    gui.add(this.spawnerOptions, "horizontalSpeed", -5, 5);
    gui.add(this.spawnerOptions, "verticalSpeed", -5, 5);
  }

  moveTo(position) {
    this.positionTarget = position;
    this.pointLight.position.copy(position);
    this.mesh.position.copy(position);
  }

  updateParticles(delta) {
    delta *= this.spawnerOptions.timeScale;
			this.tick += delta;
			if (this.tick < 0) this.tick = 0;
			if (delta > 0) {
				// this.options.position.x = this.positionTarget.x * this.spawnerOptions.horizontalSpeed;
				// this.options.position.y = this.positionTarget.y * this.spawnerOptions.verticalSpeed;
				// this.options.position.z = this.positionTarget.z * this.spawnerOptions.horizontalSpeed + this.spawnerOptions.verticalSpeed;
				// this.options.position.x = Math.sin(this.tick * this.spawnerOptions.horizontalSpeed) * 20;
				// this.options.position.y = Math.sin(this.tick * this.spawnerOptions.verticalSpeed) * 10;
				// this.options.position.z = Math.sin(this.tick * this.spawnerOptions.horizontalSpeed + this.spawnerOptions.verticalSpeed) * 5;
        this.options.position.copy(this.positionTarget);
				for (let x = 0; x < this.spawnerOptions.spawnRate * delta; x++) {
					// Yep, that's really it.	Spawning particles is super cheap, and once you spawn them, the rest of
					// their lifecycle is handled entirely on the GPU, driven by a time uniform updated below
					this.particleSystem.spawnParticle(this.options);
				}
			}
			this.particleSystem.update(this.tick);
  }

  show() {
    this.mesh.visible = true;
    this.particleSystem.visible = true;

  }

  hide() {
    this.mesh.visible = false;
    this.particleSystem.visible = false;
  }
}
