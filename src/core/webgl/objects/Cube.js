
import THREE from 'three';

export default class Cube extends THREE.Object3D {
  constructor(width,color) {
    super();
    this.width = (width !== undefined) ? width : 10;
    this.color = (color !== undefined) ? color : 0x00ff00;
    this.geom = new THREE.BoxGeometry(this.width , this.width , this.width);
    this.mat = new THREE.MeshBasicMaterial({
      color: this.color,
      wireframe: false
    });
    this.mesh = new THREE.Mesh(this.geom, this.mat);
    this.mesh.name = "Cube";

    this.add(this.mesh);
  }

  update() {
    this.rotation.x += 0.01;
    this.rotation.z += 0.01;
  }
}
