import THREE from 'three';
import dat from 'dat-gui';

let gui;

export default class ParticlesBackground extends THREE.Object3D {
  constructor(width) {
    super();
    // width = 100;
    let nbSegments = 16;
    let geometry1 = new THREE.BoxGeometry( width, width, width, 16, 16, 16 );
    // let vertices = geometry1.vertices;
    // let positions = new Float32Array( vertices.length * 3 );
    // let colors = new Float32Array( vertices.length * 3 );
    // let sizes = new Float32Array( vertices.length );
    // let vertex;
    // let color = new THREE.Color();
    // let particleSize = 3;
    //
    // for ( let i = 0, l = vertices.length; i < l; i ++ ) {
    //   vertex = vertices[ i ];
    //   vertex.toArray( positions, i * 3 );
    //   color.setHSL( 0.01 + 0.1 * ( i / l ), 1.0, 0.5 )
    //   color.toArray( colors, i * 3 );
    //   sizes[ i ] = particleSize * 0.5;
    // }
    //
    // let geometry = new THREE.BufferGeometry();
    // geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
    // geometry.addAttribute( 'customColor', new THREE.BufferAttribute( colors, 3 ) );
    // geometry.addAttribute( 'size', new THREE.BufferAttribute( sizes, 1 ) );
    //
    let textureLoader = new THREE.TextureLoader();
    let texture = textureLoader.load('textures/particle.png');
    let material = new THREE.PointsMaterial({size: 1
                                          ,color:0x84afb6
                                            // ,map:texture
    });

    this.particles = new THREE.Points( geometry1, material );
    this.add( this.particles );

    /*
    //fond
    let lineGeometry = new THREE.Geometry();
    for (let i = 0; i < nbSegments; i++) {
      lineGeometry.vertices.push(new THREE.Vector3( -width/2, width*i/nbSegments-width/2, width/2 ));
      lineGeometry.vertices.push(new THREE.Vector3( width/2, width*i/nbSegments-width/2, width/2 ));
    }
    let line = new THREE.Line( lineGeometry, new THREE.LineBasicMaterial( { color: 0x416669, opacity: 0.5 } ) );
    this.add( line );

    lineGeometry = new THREE.Geometry();
    for (let i = 0; i < nbSegments; i++) {
      lineGeometry.vertices.push(new THREE.Vector3( width*i/nbSegments-width/2, -width/2, width/2 ));
      lineGeometry.vertices.push(new THREE.Vector3( width*i/nbSegments-width/2, width/2, width/2 ));
    }
    line = new THREE.Line( lineGeometry, new THREE.LineBasicMaterial( { color: 0x416669, opacity: 0.5 } ) );
    this.add( line );


    //gauche
    lineGeometry = new THREE.Geometry();
    for (let i = 0; i < nbSegments; i++) {
      lineGeometry.vertices.push(new THREE.Vector3( width/2, width*i/nbSegments-width/2, width/2 ));
      lineGeometry.vertices.push(new THREE.Vector3( width/2, width*i/nbSegments-width/2, -width/2 ));
    }
    line = new THREE.Line( lineGeometry, new THREE.LineBasicMaterial( { color: 0x416669, opacity: 0.5 } ) );
    this.add( line );

    lineGeometry = new THREE.Geometry();
    for (let i = 0; i < nbSegments; i++) {
      lineGeometry.vertices.push(new THREE.Vector3( width/2, -width/2, width*i/nbSegments-width/2 ));
      lineGeometry.vertices.push(new THREE.Vector3( width/2, width/2, width*i/nbSegments-width/2 ));
    }
    line = new THREE.Line( lineGeometry, new THREE.LineBasicMaterial( { color: 0x416669, opacity: 0.5 } ) );
    this.add( line );
    */

  }

  update(delta) {
  }
}
