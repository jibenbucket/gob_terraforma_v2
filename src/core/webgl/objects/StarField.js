
import THREE from 'three';

export default class StarField extends THREE.Object3D {
  constructor(radius) {
    super();
    this.geom = new THREE.SphereGeometry(radius, 32, 32);
    this.mat = new THREE.MeshBasicMaterial({
      map: new THREE.TextureLoader().load( "images/textures/galaxy_starfield.png"),
      side: THREE.BackSide
    });
    this.mesh = new THREE.Mesh(this.geom, this.mat);
    this.mesh.name = "StarField";

    this.add(this.mesh);
  }

  update() {
    this.rotation.x += 0.01;
    this.rotation.z += 0.01;
  }
}
