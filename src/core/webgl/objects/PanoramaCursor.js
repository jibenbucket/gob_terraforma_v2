import THREE from 'three';

const names = ['pesticides','nuclear'];
const thetas = [5.5, 3.9];
const phis = [0,6];

export default class PanoramaCursor extends THREE.Object3D {
  constructor(index) {
    super();
    this.geom = new THREE.PlaneGeometry(228 , 309);
    let textureLoader = new THREE.TextureLoader();
    let texture = textureLoader.load('textures/'+names[index]+'.png');

    this.mat = new THREE.MeshBasicMaterial({
      map: texture,
      side: THREE.DoubleSide,
      transparent: true
    });
    this.mesh = new THREE.Mesh(this.geom, this.mat);
    this.mesh.name = names[index];

    this.radius = 970;
    this.theta = thetas[index];
    this.phi = phis[index];

    this.mesh.position.x = this.radius * Math.sin(this.theta) * Math.cos(this.phi);
    this.mesh.position.y = this.radius * Math.sin(this.theta) * Math.sin(this.phi);
    this.mesh.position.z = this.radius * Math.cos(this.theta) ;
    let target = new THREE.Vector3(0,0,0);
    this.mesh.lookAt(target);
    this.scale.set(0.5,0.5,0.5);
    this.add(this.mesh);

  }
}
