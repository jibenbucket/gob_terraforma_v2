
import THREE from 'three';

export default class Plant extends THREE.Object3D {
  constructor() {
    super();

    this.sphereGeometry = new THREE.Geometry();
    this.vertices_tmp = [];
    this.risedVertices = 0;
    this.hasRised = false;

    let loader = new THREE.JSONLoader();
    loader.load( './objects/js/plant.js', ( geometry, materials ) => {
      let vl = geometry.vertices.length;
      let r = 1;

      for ( let j = 0; j < vl; j ++ ) {
        let theta = Math.random()*Math.PI*2;
        let phi = Math.random()*Math.PI*2;
        this.sphereGeometry.vertices[ j ] = new THREE.Vector3(r * Math.sin(theta) * Math.cos(phi),
                                        r * Math.sin(theta) * Math.sin(phi),
                                        r * Math.cos(theta) );
        this.vertices_tmp[ j ] = [ geometry.vertices[ j ].x, geometry.vertices[ j ].y, geometry.vertices[ j ].z, 0 ];
      }

      this.originalVertices = this.sphereGeometry.vertices;
      let material = new THREE.PointsMaterial( { size: 0.25, color: 0x804e29 });
      let mesh = new THREE.Points( this.sphereGeometry, material);
      mesh.position.set(0,0,0);
      // mesh.name = "plant";
      this.name = "plant";
      mesh.scale.x = 2; mesh.scale.y = 2; mesh.scale.z = 2;
      mesh.matrixAutoUpdate = true;
      // mesh.updateMatrix();

      // console.log(mesh.geometry.vertices);
      // console.log(this.vertices);
      this.add(mesh);
    });
  }

  update(delta) {
    if(this.children.length > 0){
      let vl = this.children[0].geometry.vertices.length;
      if(this.risedVertices < vl) {
        delta = delta < 2 ? delta : 2;
        // this.mesh.material.opactiy = 1;

        for ( let vIndex = 0; vIndex < vl; vIndex ++ ) {
          let vt = this.vertices_tmp[vIndex];
          let p = this.children[0].geometry.vertices[vIndex];
          let d = Math.abs( p.x - vt[0] ) + Math.abs( p.y - vt[1] ) + Math.abs( p.z - vt[2] );

          if ( d > 0.1 ) {
            p.x += - ( p.x - vt[0] ) / d * delta;
            p.y += - ( p.y - vt[1] ) / d * delta;
            p.z += - ( p.z - vt[2] ) / d * delta;
            // p.x += - ( p.x - vt[0] ) / d * delta * ( 0.85 - Math.random() );
            // p.y += - ( p.y - vt[1] ) / d * delta * ( 0.85 - Math.random() );
            // p.z += - ( p.z - vt[2] ) / d * delta * ( 0.85 - Math.random() );

            // p.x -= 1.5 * ( 0.50 - Math.random() ) * this.speed ;
            // p.y -= 3.0 * ( 0.25 - Math.random() ) * this.speed ;
            // p.z -= 1.5 * ( 0.50 - Math.random() ) * this.speed ;
          } else if (vt[3] == 0) {
              vt[3] = 1;
              this.risedVertices ++;
          }
        }

        this.children[0].geometry.verticesNeedUpdate = true;
      } else if (!this.hasRised){
          this.hasRised = true;
          this.children[0].material.color.setHex(0x335938);
        }
      }
    }

}
