
import THREE from 'three';
import d from 'data/datas-xp-nuclear';
import { upgradeObject } from '../../../utils/UpgradedObject';

export default class Earth extends THREE.Object3D {
  constructor(radius) {
    super();
    this.radius = radius;
    this.geom = new THREE.SphereGeometry(this.radius, 64, 64);
    this.loaderprogress = 0;
    let textureLoader = new THREE.TextureLoader();
    this.mat = new THREE.MeshPhongMaterial({
      map: textureLoader.load( "/textures/Earth_2048.jpg",
        ()=>{this.loaderprogress=1;},
        (xhr)=> {this.loaderprogress = xhr.loaded / xhr.total;}
      )
    });
    this.mat.bumpMap = textureLoader.load('textures/earthbump.jpg');
    this.mat.bumpScale = this.radius/10;
    this.mesh = new THREE.Mesh(this.geom, this.mat);
    this.mesh.name = "Earth";
    this.add(this.mesh);

    this.addData();
    this.nbPins = d.places.length;
  }

  update() {
    this.rotation.y -= 0.0003;
  }

  // height param is position from the center of sphere (used as an offset of how high above the surface we want to start drawing)
  // radius param is the radius of our earth
  latLongToVector3(lat, lon, radius, heigth) {
    let phi = (lat)*Math.PI/180;
    let theta = (lon-180)*Math.PI/180;

    let x = -(radius+heigth) * Math.cos(phi) * Math.cos(theta);
    let y = (radius+heigth) * Math.sin(phi);
    let z = (radius+heigth) * Math.cos(phi) * Math.sin(theta);

    return new THREE.Vector3(x,y,z);
  }

    // simple function that converts the density data to the markers on screen
    // the height of each marker is relative to the density.
  addData() {
    // material to use for each of our elements. Could use a set of materials to
    // add colors relative to the density. Not done here.
    for (let i = 0 ; i < d.places.length ; i++) {
      let y = parseFloat(d.places[i].lon)+180;
      let x = parseFloat(1+d.places[i].lat)*-1;

      // calculate the position where we need to start the pointer
      let pointerPosition = this.latLongToVector3(x, y, -this.radius, 0);
      // create the pointer
      let pin = new THREE.Mesh(new THREE.SphereGeometry(0.5,6,6),new THREE.MeshBasicMaterial({color: 0xff0000,opacity:1,transparent:true}));
      // position the cube correctly
      pin.position.setX(pointerPosition.x);
      pin.position.setY(pointerPosition.y);
      pin.position.setZ(pointerPosition.z);
      pin.lookAt( new THREE.Vector3(0,0,0) );
      pin.name = d.places[i].name;
      pin.updateMatrix();
      upgradeObject(pin);      //upgradeObject to add isFound and counter properties.
      // merge with the earth
      this.add(pin);
    }


  }
}
