import THREE from 'three';
import dat from 'dat-gui';

let gui;

export default class ParticlesBackground extends THREE.Object3D {
  constructor() {
    super();
    gui = new dat.GUI();
    this.tick = 0;
    this.particleSystem = new THREE.GPUParticleSystem({
      maxParticles: 250000
    });
    this.add(this.particleSystem);
    // options passed during each spawned
    this.options = {
      position: new THREE.Vector3(0,0,0),
      positionRandomness: 2000,
      velocity: new THREE.Vector3(),
      velocityRandomness: 5,
      color: 0xa8eaff,
      colorRandomness: .1,
      turbulence: 0,
      lifetime: 1000,
      size: 15,
      sizeRandomness: 10
    };
    this.spawnerOptions = {
      spawnRate: 10,
      horizontalSpeed: 0.5,
      verticalSpeed: 0.5,
      timeScale: 0.025
    };

    for (let x = 0; x < 2000; x++) {
      this.particleSystem.spawnParticle(this.options);
    }

    gui.add(this.options, "velocityRandomness", 0, 3);
    gui.add(this.options, "positionRandomness", 0, 1000);
    gui.add(this.options, "size", 1, 20);
    gui.add(this.options, "sizeRandomness", 0, 25);
    gui.add(this.options, "colorRandomness", 0, 1);
    gui.add(this.options, "lifetime", .1, 100);
    gui.add(this.options, "turbulence", 0, 1);
    gui.add(this.spawnerOptions, "spawnRate", 0, 200);
    gui.add(this.spawnerOptions, "timeScale", -1, 1);
    gui.add(this.spawnerOptions, "horizontalSpeed", -5, 5);
    gui.add(this.spawnerOptions, "verticalSpeed", -5, 5);

  }

  update(delta) {
      delta *= this.spawnerOptions.timeScale;
			this.tick += delta;
			if (this.tick < 0) this.tick = 0;
			// if (delta > 0) {
			// 	for (let x = 0; x < this.spawnerOptions.spawnRate * delta; x++) {
			// 		this.particleSystem.spawnParticle(this.options);
			// 	}
			// }
			this.particleSystem.update(this.tick);
  }
}
