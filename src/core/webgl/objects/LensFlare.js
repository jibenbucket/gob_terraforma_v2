
import THREE from 'three';

export default class LensFlare extends THREE.Group {
  constructor(h, s, l, x, y, z, distance) {
    super();
    //( 0.55, 0.9, 0.5, 5000, 0, -1000 );
    let textureLoader = new THREE.TextureLoader();
		let textureFlare0 = textureLoader.load( "textures/lensflare/lensflare0.png" );
		let textureFlare2 = textureLoader.load( "textures/lensflare/lensflare2.png" );
		let textureFlare3 = textureLoader.load( "textures/lensflare/lensflare3.png" );

    this.light = new THREE.PointLight( 0xff0000, 1.5, 20000 );
    this.light.color.setHSL( h, s, l );
    this.light.position.set( x, y, z );
    this.add( this.light );

    let flareColor = new THREE.Color( 0xff0000 );
    flareColor.setHSL( h, s, l + 0.5 );

    this.lensFlare = new THREE.LensFlare( textureFlare0, 300, 0.0, THREE.AdditiveBlending, flareColor );
    this.lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
    this.lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
    this.lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
    this.lensFlare.add( textureFlare3, 60, 0.6, THREE.AdditiveBlending );
    this.lensFlare.add( textureFlare3, 70, 0.7, THREE.AdditiveBlending );
    this.lensFlare.add( textureFlare3, 120, 0.9, THREE.AdditiveBlending );
    this.lensFlare.add( textureFlare3, 70, 1.0, THREE.AdditiveBlending );
    this.lensFlare.customUpdateCallback = lensFlareUpdateCallback;
    this.lensFlare.position.copy( this.light.position );
    this.name = "LensFlare";

    this.add(this.lensFlare);

    function lensFlareUpdateCallback( object ) {
				let f, fl = object.lensFlares.length;
				let flare;
				let vecX = -object.positionScreen.x * 2;
				let vecY = -object.positionScreen.y * 2;
				for( f = 0; f < fl; f++ ) {
					flare = object.lensFlares[ f ];
					flare.x = object.positionScreen.x + vecX * flare.distance;
					flare.y = object.positionScreen.y + vecY * flare.distance;
					flare.rotation = 0;
				}
				object.lensFlares[ 2 ].y += 0.025;
				object.lensFlares[ 3 ].rotation = object.positionScreen.x * 0.5 + THREE.Math.degToRad( 45 );
			}
  }


}
