import THREE from 'three';

export default class Panorama extends THREE.Object3D {
  constructor(urlPrefix) {
    super();
    this.width = 2048;
    let height = this.width;
    let depth = this.width;
    this.name = "Panorama";
    let urls = [ urlPrefix + "posx.png", urlPrefix + "negx.png",
    urlPrefix + "posy.png", urlPrefix + "negy.png",
    urlPrefix + "posz.png", urlPrefix + "negz.png" ];
    /*
    let urls = [ urlPrefix + "posx.jpg", urlPrefix + "negx.jpg",
    urlPrefix + "posy.jpg", urlPrefix + "negy.jpg",
    urlPrefix + "posz.jpg", urlPrefix + "negz.jpg" ];
    */
    let textureCube = new THREE.CubeTextureLoader().load(urls,(texture)=>{this.loaderprogress=1;},
        (xhr)=>{this.loaderprogress=xhr.loaded / xhr.total; });
    let shader = THREE.ShaderLib["cube"];
    shader.uniforms["tCube"].value = textureCube;
    let material = new THREE.ShaderMaterial( {
      fragmentShader: shader.fragmentShader,
      vertexShader: shader.vertexShader,
      uniforms: shader.uniforms,
      depthWrite: false,
      side: THREE.BackSide
    } );
    this.mesh = new THREE.Mesh( new THREE.BoxGeometry( this.width, height, depth ), material );
    this.mesh.position.set(0, 0, 0);
    this.add(this.mesh);
  }
}
