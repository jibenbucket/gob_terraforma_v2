import THREE from 'three';

export default class Panorama extends THREE.Object3D {
  constructor(width, height, radius, url) {
    super();
    this.name = "Panorama";
    this.geometry = new THREE.SphereBufferGeometry( radius, 60, 40 );
    this.geometry.scale( - 1, 1, 1 );
    this.video = document.createElement( 'video' );
    this.video.width = width;
    this.video.height = height;
    this.video.autoplay = true;
    // this.video.loop = true;
    this.video.src = "videos/"+url;
    this.texture = new THREE.VideoTexture( this.video );
    this.texture.minFilter = THREE.LinearFilter;
    this.texture.format = THREE.RGBFormat;
    this.material   = new THREE.MeshBasicMaterial( { map : this.texture } );
    this.mesh = new THREE.Mesh( this.geometry, this.material );
    this.add( this.mesh );
  }
}
