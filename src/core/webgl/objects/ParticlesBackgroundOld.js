import THREE from 'three';

export default class ParticlesBackground extends THREE.Object3D {
  constructor() {
    super();
    this.nbParticles = 2000;

    let geometry = new THREE.Geometry();
    this.velocities = [];
    // creating random vertices and velocities
    for (let i = 0; i < this.nbParticles; i ++ ) {
      let vertex = new THREE.Vector3();
      vertex.x = Math.random() * 2000 - 1000;
      vertex.y = Math.random() * 2000 - 1000;
      vertex.z = Math.random() * 2000 - 1000;
      let velocity = new THREE.Vector3();
      velocity.x = Math.random() * 40;
      velocity.y = Math.random() * 40;
      velocity.z = Math.random() * 40;

      geometry.vertices.push( vertex );
      this.velocities.push( vertex );
    }

    let textureLoader = new THREE.TextureLoader();
    let texture = textureLoader.load('textures/particle.png');
    this.particles = new THREE.Points( geometry, new THREE.PointsMaterial( { size: Math.random()*70
      // ,color:0x00abc9
      ,map:texture
    } ));

    this.add( this.particles );
  }

  update(delta) {
    for (let i = 0; i < this.nbParticles; i++) {
      this.particles.geometry.vertices[i].x += Math.sin(this.velocities[i].x) * delta/100;
      this.particles.geometry.vertices[i].y += Math.cos(this.velocities[i].y) * delta/100;
      this.particles.geometry.vertices[i].z += Math.tan(this.velocities[i].z) * delta/100;
    }
    this.particles.geometry.verticesNeedUpdate = true;

  }
}
