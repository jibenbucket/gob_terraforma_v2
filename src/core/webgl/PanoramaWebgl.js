import THREE from 'three';
window.THREE = THREE;
import WAGNER from '@superguigui/wagner';
import Router from 'core/Router';
import States from 'core/States';
import SoundManager from 'core/SoundManager';
import data from 'data/datas-settings';
import PanoramaCursor from './objects/PanoramaCursor';
import Video from './objects/Video';
const EffectComposer = require('three-effectcomposer')(THREE);
const GlitchPass = require('./postprocessing/GlitchPass');
const VignettePass = require('@superguigui/wagner/src/passes/vignette/VignettePass.js');


export default class Webgl {

  constructor(width, height) {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.scene = new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 1, 1100 );
    this.camera.target = new THREE.Vector3( 0, 0, 0 );

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setPixelRatio( window.devicePixelRatio );

    //Composer pour l'effet masque
    this.composer = new WAGNER.Composer( this.renderer, { useRGBA: false } );
    this.composer.setSize(width,height);

    this.vignettePass = new VignettePass({
      boost: 1,
      reduction: 1.6
    });

    //Composer pour l'effet glitch
    this.effectComposer = new EffectComposer(this.renderer);
    this.effectComposer.addPass(new EffectComposer.RenderPass(this.scene, this.camera));
    this.glitchPass = new THREE.GlitchPass();
    this.soundManager = SoundManager.getInstance();

    this.videoEl = new Video(this.width, this.height, 500, 'panorama.mp4');
    this.scene.add(this.videoEl);

    this.cursors = [];
    this.cursors[0] = new PanoramaCursor(0);
    this.scene.add(this.cursors[0]);
    this.cursors[1] = new PanoramaCursor(1);
    this.scene.add(this.cursors[1]);

    this.videoEl.video.pause();
    this.isReady = false;
    this.loaderProgress = 0;
    this.isRendering = true;
    this.isGoingOnExp = false;
  }

  onReady() {
        this.glitchDelay = 5000;
        this.timeOut();
  }
  resize(width, height){
    this.width = width;
    this.height = height;
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
    if (this.composer) {
      this.composer.setSize(width, height);
    }

  }

  resumeRender() {
      this.isRendering = true;
      this.timeOut();
  }

  pauseRender() {
    this.isRendering = false;
  }

  timeOut() {
    if(this.isRendering) {
      window.setTimeout(()=>{
        this.addGlitch();
        this.timeOut();
      },this.glitchDelay);
    }
  }

  render() {
    if(this.isRendering){
      if(!this.isReady) {
        if (this.videoEl.video.readyState > 1) {
          if(!localStorage.oxyrate){
            localStorage.totalMoney = 0;
            localStorage.naturalizationRate = 0;
            localStorage.oxyrate = Math.trunc(Math.random()*10)/10;
            localStorage.hydrorate = Math.trunc(Math.random()*10)/100;
            localStorage.terrarate = Math.trunc(Math.random()*100)/100;
          }
          this.start = (parseFloat(localStorage.naturalizationRate)+0.1)*this.videoEl.video.duration;
          localStorage.naturalizationRate = parseFloat(localStorage.totalMoney)/data["maximumMoney"];
          this.end = (parseFloat(localStorage.naturalizationRate)+0.1)*this.videoEl.video.duration;
          this.videoEl.video.currentTime = this.start;
          this.loaderProgress = 1;
          this.isReady = true;
          // this.videoEl.video.play();
        }
      } else {
        this.update();

        this.renderer.setSize(this.width, this.height);
        this.renderer.render(this.scene, this.camera);
        this.composer.reset();
        this.composer.render(this.scene, this.camera);
        this.composer.pass(this.vignettePass);
        this.composer.toScreen();
        this.effectComposer.render();
      }
    }
  }

  update() {
    if(this.videoEl.video.currentTime >= this.end) {
      this.videoEl.video.pause();
      if(States && States.pesticidesfinished && States.nuclearfinished) {
        setTimeout(()=>{
          Router.go({ name: 'end'});
        }, 3000);
      }
    }
  }

  addGlitch(){
    // this.glitchSound.play();
    if(!this.isRendering) {
      return;
    }
    let duration = 150;

    let glitchPass = this.glitchPass;
    glitchPass.curF = 1;
    glitchPass.enabled = true;
    window.setTimeout(function(){
      glitchPass.enabled = false;
    },duration);
    this.effectComposer.addPass( glitchPass );
    glitchPass.renderToScreen = true;
    this.soundManager.glitch.play();
  }

  animateTo(xpIndex, xpName) {
    // this.camera.target = this.cursors[xpIndex].mesh.position.clone();
    this.isGoingOnExp = true;
    TweenMax.to(this.camera.target,0.5,{
      x:this.cursors[xpIndex].mesh.position.x,
      y:this.cursors[xpIndex].mesh.position.y+10,
      z:this.cursors[xpIndex].mesh.position.z,
      onUpdate:()=>{
        this.camera.lookAt(this.camera.target);
        this.camera.updateProjectionMatrix();
      },
      onComplete:()=>{
        TweenMax.to(this.vignettePass.params,0.2,{boost:4});
        TweenMax.to(this.camera,0.2,{fov:30,
          onUpdate:()=>{
            this.camera.updateProjectionMatrix();
          }
          ,onComplete:()=>{
            this.addGlitch();
            this.isRendering = false;
            Router.go({
              name: 'intro-'+xpName//data['xp'][xpIndex]['pathName']
            });
          }
        });
      }
    });

    // this.camera.target = this.usineCursor.position;
    // this.camera.lookAt(this.camera.target);
  }
}
