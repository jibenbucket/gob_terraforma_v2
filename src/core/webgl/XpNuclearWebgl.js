import THREE from 'three';
window.THREE = THREE;
import Emitter from 'core/Emitter';
import {
  SHOW_INFOS_POP
} from 'config/messages';
import Earth from './objects/Earth';
import Cube from './objects/Cube';
import LensFlare from './objects/LensFlare';
import StarField from './objects/StarField';
import TweenMax from 'gsap';

const searchRadius = 10;  // radius du viseur pour la position des pins
const distanceStep = 25*25;
const counterStep = 50;   // step a atteindre pour montrer l'info. Le counter augmente
                          // qd la position est dans le searchRadius. (voir update())
const cameraOffset = 50;

export default class XpNuclearWebgl {

  constructor(width, height) {
    this.params = {
      //x: 1, y:500, z:1
    };

    this.scene = new THREE.Scene();
    let light = new THREE.AmbientLight( 0x404040 , 0.75); // soft white light
    // this.scene.add( light );

    this.camera = new THREE.PerspectiveCamera(30, width / height, 1, 4000);
    this.camera.position.set(0,0, 300);
    this.camera.target = new THREE.Vector3( 0, 0, 0 );
    this.camera.lookAt(this.camera.target);

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(width, height);
    this.renderer.setClearColor(0x262626);
    this.renderer.alpha = true;

    this.earth = new Earth(100);
    this.earth.position.set(0, 0, 0);
    this.scene.add(this.earth);

    this.directionalLight = new THREE.DirectionalLight( 0x84f0f0, 0.5 );
    this.directionalLight.position.set( this.camera.position.x, this.camera.position.y, this.camera.position.z );
    this.directionalLight.target.x = this.camera.target.x; this.directionalLight.target.y = this.camera.target.y; this.directionalLight.target.z = this.camera.target.z;
    this.scene.add( this.directionalLight );

    this.starField = new StarField(2000);
    this.scene.add(this.starField);

    let sunSpotLight = new THREE.SpotLight( 0x757575, 0.1 ); //THREE.DirectionalLight(hex, intensity)
    sunSpotLight.position.set( this.earth.radius*2, this.earth.radius*2, this.camera.position.z );
    sunSpotLight.target.x = this.camera.target.x; sunSpotLight.target.y = this.camera.target.y; sunSpotLight.target.z = this.camera.target.z;
    this.params.y = 500;//Math.sqrt((this.earth.radius*2)*(this.earth.radius*2) + (this.earth.radius*2)*(this.earth.radius*2) + (this.earth.radius*2)*(this.earth.radius*2));
    sunSpotLight.penumbra = 0;
    sunSpotLight.decay = 1;
    sunSpotLight.distance = 500;
    sunSpotLight.penumbra = 0;
    sunSpotLight.decay = 1;
    sunSpotLight.angle = 1;
    sunSpotLight.intensity = 1;
    this.scene.add( sunSpotLight );

    let secondSunSpotLight = new THREE.SpotLight( 0x757575, 0.1 );
    secondSunSpotLight.position.set( -this.earth.radius*2, -this.earth.radius*2, this.camera.position.z );
    secondSunSpotLight.target.x = this.camera.target.x; secondSunSpotLight.target.y = this.camera.target.y; secondSunSpotLight.target.z = this.camera.target.z;
    secondSunSpotLight.distance = 500;//Math.sqrt((this.earth.radius*2)*(this.earth.radius*2) + (this.earth.radius*2)*(this.earth.radius*2) + (this.earth.radius*2)*(this.earth.radius*2));
    secondSunSpotLight.penumbra = 0;
    secondSunSpotLight.decay = 1;
    secondSunSpotLight.angle = 1;
    secondSunSpotLight.intensity = 1;
    this.scene.add( secondSunSpotLight );

    this.zStep = this.earth.radius - this.earth.radius*1/10;  // si z atteint un certain step alors il peut etre dans le viseur
    this.isShowingInfo = false;

    this.targetContainer;
    this.circleSvg;
    this.flecheSvg;
    this.nuclearSvg;

    this.progress = 0;
    this.isRendering = true;
  }

  setProgressFromLocalStorage(){
    this.infosIndexes = JSON.parse(localStorage.getItem('xpNuclearInfosIndexes'));
    if(this.infosIndexes){
      for(let i=0; i < this.infosIndexes.length; i++){
        this.earth.children[1+this.infosIndexes[i]].isFound = true;
        this.earth.children[1+this.infosIndexes[i]].material.opacity = 1;
        this.earth.children[1+this.infosIndexes[i]].material.color.setHex(0x801212);
        this.progress ++;
      }
      // this.progress = this.infosIndexes.length;
    } else {
      this.infosIndexes = new Array();
    }
  }

  saveProgresToLocalStorage(){
    localStorage.setItem('xpNuclearInfosIndexes',JSON.stringify(this.infosIndexes));
  }

  resize(width, height) {
    this.width = width;
    this.height = height;

    if (this.composer) {
      this.composer.setSize(width, height);
    }

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
  }

  resumeRender() {
    this.isRendering = true;
  }

  pauseRender() {
    this.isRendering = false;
  }
  getMax() {
    return this.earth.nbPins;
  }

  render(isInteracting) {
    if(this.isRendering) {
      this.update();
      this.renderer.render(this.scene, this.camera);
      // Let the earth rotate slowly like orbit
      if(!isInteracting)
        this.earth.update();
    }
  }

  update() {
    let isNearPin = false;
    this.distance = distanceStep*2;
    for(let i = 0; i < this.earth.nbPins; i ++) {
      if(!this.earth.children[1+i].isFound) {
        let position = new THREE.Vector3();
        position.setFromMatrixPosition( this.earth.children[1+i].matrixWorld );
        let distance = position.x*position.x + position.y*position.y + (position.z-this.earth.radius)*(position.z-this.earth.radius);
        if(distance < this.distance)
          this.distance = distance;
        if(distance <= distanceStep) {  // pour éviter de faire Racine carré qui coute en ressources
          isNearPin = true;
          this.earth.children[1+i].counter ++;
          this.earth.children[1+i].material.opacity = (distanceStep - distance) / distanceStep;
          if(this.earth.children[1+i].material.opacity > 0.99) {
              this.earth.children[1+i].counter ++;
              if (this.earth.children[1+i].counter > counterStep) {
                this.showInfo(i,position);
              }
          } else {
            this.earth.children[1+i].counter = 0;
          }
        } else {
          if(isNearPin == false) {
            // this.nuclearSvg.style.opacity = 0.1;
          }
          this.earth.children[1+i].material.opacity = 0.01;
        }
      }
    }
    TweenMax.set(this.nuclearSvg,{opacity:(distanceStep - this.distance) / distanceStep+0.1});
    TweenMax.set(this.flecheSvg,{scale: 1+0.2+0.2*(distanceStep - this.distance) / distanceStep });
  }

  showInfo(index,position) {
    // this.progress ++;
    this.infosIndexes.push(index);
    this.progress = this.infosIndexes.length;
    this.infoIndex = index;
    this.earth.children[1+index].isFound = true;
    this.earth.children[1+index].material.color.setHex(0x801212);
    Emitter.emit(SHOW_INFOS_POP);
    // let tl = new TimelineMax({autoplay:true, delay:0.5});
    // tl
    // .set(this.nuclearSvg,{opacity:0.1})
    // .set(this.circleSvg,{opacity:0})
    //.to(this.flecheSvg,0.4,{scale: 0.5, onComplete:()=>{
    TweenMax.to(this.flecheSvg,0.4,{scale: 0.5, onComplete:()=>{
      this.isShowingInfo = true;
      TweenMax.to(this.camera.position,0.5,{x:position.x-cameraOffset});
      TweenMax.to(this.camera.position,0.5,{y:position.y});
      TweenMax.to(this.camera.position,0.5,{z:position.z+cameraOffset,onUpdate:()=>{
                          this.camera.target = position;
                          this.camera.lookAt(this.camera.target);
                          this.camera.updateProjectionMatrix();
                      }});
      }});
  }

  resetCameraPosition() {
    this.isShowingInfo = false;
    TweenMax.set(this.nuclearSvg,{opacity:0.1});
    TweenMax.set(this.circleSvg,{opacity:1});
    TweenMax.to(this.flecheSvg,0.5,{scale: 1});
    TweenMax.to(this.camera.position,0.5,{x:0});
    TweenMax.to(this.camera.position,0.5,{y:0});
    TweenMax.to(this.camera.position,0.5,{z:300,onUpdate:()=>{
                        this.camera.target = new THREE.Vector3( 0, 0, 0 );
                        this.camera.lookAt(this.camera.target);
                        this.camera.updateProjectionMatrix();
                    }});
  }
}
