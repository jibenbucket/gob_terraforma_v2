import VueRouter from 'vue-router';
import Emitter from 'core/Emitter';

import LandingPageComponent from 'containers/LandingPage';
import PanoramaComponent from 'containers/Panorama';
import XpNuclearComponent from 'containers/XpNuclear';
import XpPesticidesComponent from 'containers/XpPesticides';
import IntroComponent from 'containers/VideoIntro';
import VideoNuclearComponent from 'containers/VideoNuclear';
import VideoPesticidesComponent from 'containers/VideoPesticide';
import RessourcesComponent from 'containers/BuyRessource';
import UncompatibleComponent from 'containers/Uncompatible';
import EndComponent from 'containers/End';

import {
  ROUTER_ROUTE_CHANGE
} from 'config/messages';

Vue.use(VueRouter);

class Router extends VueRouter {

  constructor() {

    super({
      hashbang: false,
      pushState: true,
      history: true,
      abstract: false,
      saveScrollPosition: false,
      transitionOnLoad: false
    });

    this.path = '/';
    this.firstRoute = true;
    this.routeTimeout = null;


    this.map({

      '/': {
        name: "landing",
        component: LandingPageComponent
      },
      '/intro': {
        name: "intro",
        component: IntroComponent
      },
      '/panorama': {
        name: "panorama",
        component: PanoramaComponent
      },
      '/intro-nuclear': {
        name: "intro-nuclear",
        component: VideoNuclearComponent
      },
      '/xp-nuclear': {
        name: "xp-nuclear",
        component: XpNuclearComponent
      },
      '/intro-pesticides': {
        name: "intro-pesticides",
        component: VideoPesticidesComponent
      },
      '/xp-pesticides': {
        name: "xp-pesticides",
        component: XpPesticidesComponent
      },
      '/buy-resources': {
        name: "buy-resources",
        component: RessourcesComponent
      },
      '/uncompatible': {
        name: "uncompatible",
        component: UncompatibleComponent
      },
      '/end': {
        name: "end",
        component: EndComponent
      }
    });

    this.afterEach( ({ to, from }) => {

      Emitter.emit(ROUTER_ROUTE_CHANGE, { to, from });

    });
  }
}

export default new Router;
