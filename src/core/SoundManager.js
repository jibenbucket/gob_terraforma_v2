class SoundManager {

  constructor(){
    this.instance;
    //load sounds
    this.ambient = document.getElementById("ambient");
    this.resource_enter = document.getElementById("resource_enter");
    this.resource_out = document.getElementById("resource_out");
    this.moneygain = document.getElementById("moneygain");
    this.revealresources = document.getElementById("revealresources");
    this.transitionmoney = document.getElementById("transitionmoney");
    this.resourceok = document.getElementById("resourceok");
    this.btn_enter = document.getElementById("btn_enter");
    this.glitchy = document.getElementById("glitchy");
    this.btn_out = document.getElementById("btn_out");
    this.btn_click = document.getElementById("btn_click");
    this.breath = document.getElementById("breath");
    this.transition = document.getElementById("transition");
    this.glitch = document.getElementById("glitch");
    this.nuclear =  document.getElementById("nuclear_audio");
    this.pesticides =  document.getElementById("pesticides_audio");
    this.resources =  document.getElementById("resources_audio");

    this.isMuted = false;
  }

  getInstance(){
    if(!this.instance){
    this.instance = new SoundManager();
    }
    return this.instance;
  }

  mute(){
    this.ambient.muted = true;
    this.resource_enter.muted = true;
    this.resource_out.muted = true;
    this.moneygain.muted = true;
    this.revealresources.muted = true;
    this.transitionmoney.muted = true;
    this.resourceok.muted = true;
    this.btn_enter.muted = true;
    this.glitchy.muted = true;
    this.btn_out.muted = true;
    this.btn_click.muted = true;
    this.breath.muted = true;
    this.transition.muted = true;
    this.glitch.muted = true;
    this.nuclear.muted = true;
    this.pesticides.muted = true;
    this.resources.muted = true;
  }

  demute(){
    this.ambient.muted = false;
    this.resource_enter.muted = false;
    this.resource_out.muted = false;
    this.moneygain.muted = false;
    this.revealresources.muted = false;
    this.transitionmoney.muted = false;
    this.resourceok.muted = false;
    this.btn_enter.muted = false;
    this.glitchy.muted = false;
    this.btn_out.muted = false;
    this.btn_click.muted = false;
    this.breath.muted = false;
    this.transition.muted = false;
    this.glitch.muted = false;
    this.nuclear.muted = false;
    this.pesticides.muted = false;
    this.resources.muted = false;
  }

  onOffSound() {
      this.isMuted = !this.isMuted;
      if(this.isMuted)
        this.mute();
      else
        this.demute();
  }
}

export default new SoundManager();
