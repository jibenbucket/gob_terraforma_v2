'use strict';

import './styles.scss';

import Emitter from 'core/Emitter';
import Router from 'core/Router';

import scrambleText from 'utils/ScrambleTextPlugin';

import {
  WINDOW_RESIZE,
  SHOW_INFOS_POP,
  HIDE_INFOS_POP,
  HIDE_POPUP_CHOICES
} from 'config/messages';

import imageLoader from 'utils/ImageLoader';

import XpProgressComponent from 'components/XpProgressBar';
import PopUpComponent from 'components/PopUpNuclear';
import PopUpConfirmationComponent from 'components/PopUpConfirmation';
import LoaderProgressComponent from 'components/LoaderProgressBar';
import LoaderInteractionComponent from 'components/LoaderInteractionNuclear';
import FooterComponent from 'components/Footer';
import States from 'core/States';
import SoundManager from 'core/SoundManager';

import Webgl from 'core/webgl/XpNuclearWebgl';
import raf from 'raf';
import 'gsap';
import d from 'data/datas-xp-nuclear';

let MAXIMUM_MONEY = d['maximumMoney'];
let SCROLL_MINIMUM = 20;
let SCROLL_MAXIMUM = 55;

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      loaderprogress : 0,
      progress: 0,
      step    : 3,
      max     : 0,
      accomplishment : 0,
      date: "01/01/1995",
      title: "Titre",
      location: "Mexico",
      lat: 0,
      lon: 0.2,
      image: "Helloooooo",
      text: "Le texte",
      textPop: "",
      popType: "",
      id: 0
    };
  },

  created() {

    this.mouseConfig = {
      isUserInteracting : false,
      newX : 0,
      newY : 0,
      lastMouseX : 0,
      lastMouseY : 0,
      deltaX : 0,
      deltaY : 0
    };

    //webgl settings
    this.webgl = new Webgl(window.innerWidth, window.innerHeight);
    this.progress = this.webgl.progress;
    this.max = this.webgl.getMax();
    this.loaderprogress = 0;
    this.start = false;
    // this.max = 5;
    this.bind();
  },

  ready() {
    let three = document.getElementById('three-container');
    three.appendChild(this.webgl.renderer.domElement);
    this.loader = document.getElementById('loader-container');
    this.startButton = document.getElementById('progress-start');
    this.startButton.addEventListener('click', this.onStartButton, false);
    this.webgl.circleSvg = document.getElementById('circle');
    this.webgl.flecheSvg = document.getElementById('fleche');
    this.webgl.nuclearSvg = document.getElementById('nuclear');
    this.targetContainer = document.getElementById('target-container');
    this.webgl.targetContainer = this.targetContainer;
    this.imageEl = document.getElementById('image');

    this.popUpContainer = document.getElementById('pop-up-nuclear-container');
    this.popUpConfirm = document.getElementById('pop-up-confirm-container');
    this.popUpConfirmBg = document.getElementById('pop-up-background');
    this.popUpInfosBg = document.getElementById('pop-up-infos-background');
    this.popUpChoiceBtnConfirm = document.getElementById('pop-btn-confirm');
    this.popUpChoiceBtnCancel = document.getElementById('pop-btn-cancel');
    this.popUpChoiceTitle = document.getElementById('pop-up-choice-title');
    this.popUpInfoTitle = document.getElementById('title');
    this.popUpCorners = document.getElementsByClassName('corner-pop-info');
    this.generateTl();
    this.animate();

    this.soundManager = SoundManager.getInstance();
    window.onblur = ()=> {
      SoundManager.mute();
    };
    window.onfocus = ()=> {
      SoundManager.demute();
    };
    SoundManager.demute();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
    * Binding & Events
    */

    bind() {
      this.animate = this.animate.bind(this);
    },

    generateTl() {
      this.tlShowPopUpChoices = new TimelineMax( { paused: true });
      this.tlShowPopUpChoices
      .to([this.popUpConfirmBg, this.popUpConfirm], 0.1, { display: "block", ease: Expo.easeIn })
      .fromTo([this.popUpConfirmBg, this.popUpConfirm], 0.5, { opacity: 0 } , { opacity : 0.94 }, Expo.easeIn, "-=0.1");

      this.tlShowPopInfos = new TimelineMax( { paused: true });
      this.tlShowPopInfos
      // .to(this.popUpCorners, 0.5, {display: "block", ease: Expo.easeIn})
      .to([this.popUpInfosBg, this.popUpContainer], 0.1, { display: "block", ease: Expo.easeIn })
      // .fromTo(this.popUpCorners, 0.5, { scale: 0, x: "-50%", y:"-50%"}, { scale: 1, x: "-50%", y:"-50%", ease: Expo.easeIn}, "-=2")
      .fromTo([this.popUpInfosBg, this.popUpContainer], 0.5, { opacity: 0 } , { opacity : 0.94, onStart:()=>{
        TweenMax.to(this.popUpInfoTitle, 1, { scrambleText: { text: this.title, chars: "abcdefghijklmnopqrstuvwxyz-°(){}&@_", speed: 1}}, "-=1");
      }}, Expo.easeIn);

      this.tlHidePopUpInfos = new TimelineMax( { paused: true });
      this.tlHidePopUpInfos
      .fromTo([this.popUpInfosBg, this.popUpContainer], 0.5, { opacity: 0.94 } , { opacity : 0 }, Expo.easeIn)
      .to([this.popUpInfosBg, this.popUpContainer], 0.1, { display: "none", ease: Expo.easeIn, onComplete:()=>{
        if (this.progress == this.step) {
          //show pop up first step
          this.showPopUpChoices("firstStep", this.webgl.progress);
        } else if (this.progress >= this.max/2) {
          this.showPopUpChoices("halfStep", this.webgl.progress);
        } else if (this.progress >= this.max) {
          this.showPopUpChoices("missionDone", this.webgl.progress);
        }
      }});
      // .fromTo(this.popUpContainer, 0.7, { scale: 1, x: "-50%", y:"-50%" }, { scale: 0, x: "-50%", y:"-50%", ease: Expo.easeIn, onComplete:()=>{
      //   if (this.progress == 1) {
      //     //show pop up first step
      //     this.showPopUpChoices("firstStep", this.webgl.progress);
      //
      //   }
      // }}, "-=0.1");

    },

    addEventListeners() {
      window.onblur = ()=> {
        this.webgl.pauseRender();
        SoundManager.mute();
      };
      window.onfocus = ()=> {
        this.webgl.resumeRender();
        SoundManager.demute();
      };
      Emitter.on(WINDOW_RESIZE, this.onWindowResize);
      Emitter.on(SHOW_INFOS_POP, this.showPopUpInfos);
      Emitter.on(HIDE_POPUP_CHOICES, this.hidePopUpChoices);
      Emitter.on(HIDE_INFOS_POP, this.popDown);
      document.addEventListener( 'mousedown', this.onDocumentMouseDown, false );
      document.addEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.addEventListener( 'mouseup', this.onDocumentMouseUp, false );
      document.addEventListener( 'mousewheel', this.onDocumentMouseWheel, false );
      // document.addEventListener('keypress', this.onKeyPress, false);
    },

    removeEventListeners() {
      Emitter.off(WINDOW_RESIZE, this.onWindowResize);
      Emitter.off(SHOW_INFOS_POP, this.showPopUpInfos);
      Emitter.off(HIDE_POPUP_CHOICES, this.hidePopUpChoices);
      document.removeEventListener( 'mousedown', this.onDocumentMouseDown, false );
      document.removeEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.removeEventListener( 'mouseup', this.onDocumentMouseUp, false );
      document.removeEventListener( 'mousewheel', this.onDocumentMouseWheel, false );
      // document.removeEventListener('keypress', this.onKeyPress, false);

    },
    animate() {
      raf(this.animate);
      if(this.start == false) {
        this.loaderprogress = this.webgl.earth.loaderprogress;
        if (this.loaderprogress >= 1) {
          TweenMax.to(this.startButton,0.3,{opacity:1});
        }
      } else {
        // Pour les components
        this.progress = this.webgl.progress;
        this.accomplishment = Math.round(this.progress/this.max*100);
        this.webgl.render(this.isUserInteracting);
      }
    },

    onStartButton ( event ) {
        if(!this.start) {
          this.soundManager.nuclear.play();
          this.webgl.render(this.isUserInteracting);
          TweenMax.to(this.loader,0.3,{opacity:0,onComplete:()=>{
            this.start = true;
            TweenMax.set(this.loader,{display:'none'});
          }});
          this.addEventListeners();
          this.webgl.resize(window.innerWidth, window.innerHeight);
          this.webgl.setProgressFromLocalStorage();
          this.progress = this.webgl.progress;
          this.startAccomplishment = Math.round(this.progress/this.max*100);
          this.accomplishment = this.startAccomplishment;
        } else {
          TweenMax.to(this.loader,0.3,{opacity:0,onComplete:()=>{
            TweenMax.set(this.loader,{display:'none'});
          }});
        }
    },

    onHelpButton() {
      TweenMax.set(this.loader,{display:'inherit'});
      TweenMax.to(this.loader,0.3,{opacity:1});
    },

    onDocumentMouseDown( event ) {
      event.preventDefault();
      if(this.webgl.isShowingInfo == false) {
        this.mouseConfig.isUserInteracting = true;
        this.mouseConfig.lastMouseX = event.clientX;
        this.mouseConfig.lastMouseY = event.clientY;
        this.mouseConfig.onPointerDownLon = this.mouseConfig.lon;
        this.mouseConfig.onPointerDownLat = this.mouseConfig.lat;
      }
    },

    onDocumentMouseMove( event ) {
      if(this.webgl.isShowingInfo == false) {
        if ( this.mouseConfig.isUserInteracting === true ) {
          this.mouseConfig.newX = event.clientX;
          this.mouseConfig.newY = event.clientY;
          this.mouseConfig.deltaX = this.mouseConfig.newX - this.mouseConfig.lastMouseX;
          this.mouseConfig.deltaY = this.mouseConfig.newY - this.mouseConfig.lastMouseY;
          this.mouseConfig.lon = ( this.mouseConfig.onPointerDownPointerX - event.clientX ) * 0.1 + this.mouseConfig.onPointerDownLon;
          this.mouseConfig.lat = ( event.clientY - this.mouseConfig.onPointerDownPointerY ) * 0.1 + this.mouseConfig.onPointerDownLat;
          this.uploadEarth();
        }
      }
    },

    onDocumentMouseUp( event ) {
      if(this.webgl.isShowingInfo == false) {
        this.mouseConfig.isUserInteracting = false;
      }
    },

    onDocumentMouseWheel( event ) {
      // WebKit
      if (event.wheelDeltaY) {
        this.webgl.camera.fov -= event.wheelDeltaY * 0.05;
        if(this.webgl.camera.fov < SCROLL_MINIMUM || this.webgl.camera.fov > SCROLL_MAXIMUM){
          this.webgl.camera.fov += event.wheelDeltaY * 0.05;
        }
      // Opera / Explorer 9
      } else if (event.wheelDelta) {
        this.webgl.camera.fov -= event.wheelDelta * 0.05;
        if(this.webgl.camera.fov < SCROLL_MINIMUM || this.webgl.camera.fov > SCROLL_MAXIMUM){
          this.webgl.camera.fov += event.wheelDeltaY * 0.05;
        }
      // Firefox
      } else if(event.detail) {
        this.webgl.camera.fov += event.detail * 1.0;
        if(this.webgl.camera.fov < SCROLL_MINIMUM || this.webgl.camera.fov > SCROLL_MAXIMUM){
          this.webgl.camera.fov -= event.detail * 1.0;
        }
      }

      this.webgl.camera.updateProjectionMatrix();
    },

    onWindowResize() {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.webgl.resize(this.width, this.height);
    },

    onKeyPress() {
      this.popDown();
    },

    uploadEarth() {
      // this.mCurrentOrientation.fromEuler(gradosX, gradosY, 0);
      // this.mCurrentOrientation.normalize();
      // Quaternion q = new Quaternion(this.mStartOrientation);
      // q.multiply(this.mCurrentOrientation);
      // this.mEmpty.setOrientation(q);
      this.webgl.earth.rotation.y -= (this.webgl.earth.rotation.y - (this.webgl.earth.rotation.y + this.mouseConfig.deltaX / 250))/2;
      this.webgl.earth.rotation.x -= (this.webgl.earth.rotation.x - (this.webgl.earth.rotation.x + this.mouseConfig.deltaY / 250))/2;

      this.webgl.starField.rotation.y -= (this.webgl.starField.rotation.y - (this.webgl.starField.rotation.y + this.mouseConfig.deltaX / 250))/2;
      this.webgl.starField.rotation.x -= (this.webgl.starField.rotation.x - (this.webgl.starField.rotation.x + this.mouseConfig.deltaY / 250))/2;

      this.mouseConfig.lastMouseX = this.mouseConfig.newX;
      this.mouseConfig.lastMouseY = this.mouseConfig.newY;
    },

    showPopUpInfos() {
      this.mouseConfig.isUserInteracting = false;
      this.setInfoContent();
      this.imageEl.style.backgroundImage = "url("+this.image+")";
      this.tlShowPopInfos.restart();
    },

    popDown() {
      this.webgl.resetCameraPosition();
      this.tlHidePopUpInfos.restart();
    },

    setInfoContent() {
      this.title = d.places[this.webgl.infoIndex].name;
      this.location = d.places[this.webgl.infoIndex].location;
      this.lat = d.places[this.webgl.infoIndex].lat;
      this.lon = d.places[this.webgl.infoIndex].lon;
      this.image = d.places[this.webgl.infoIndex].image;
      this.text = d.places[this.webgl.infoIndex].descr;
      this.date = d.places[this.webgl.infoIndex].date;
      this.id = parseInt(Math.random() * (21 - 1) + 1);
    },

    goHomeBtn() {
      this.webgl.saveProgresToLocalStorage();
      States.currentMoney = MAXIMUM_MONEY * (this.accomplishment-this.startAccomplishment)/100;
      States.nbDiscovered = this.progress;
      States.nbInfos = this.max;

      if (this.webgl.progress < this.step){
        this.showPopUpChoices("zeroStep", this.webgl.progress);
        // this.webgl.isShowingInfo == true;
        // // display warning experience not finish
        // // this.textPop = "ARE YOU REALLY SURE ??";
        // this.tlShowPopUpChoices.restart();
      } else {
        ///go to buy resource
        States.nuclearfinished = true;
        localStorage.setItem('nuclearfinished',JSON.stringify(States.nuclearfinished));
        Router.go({ name: 'buy-resources'});
      }
    },
    showPopUpChoices(type, userProgress){
      this.webgl.saveProgresToLocalStorage();
      States.currentMoney = MAXIMUM_MONEY * (this.accomplishment-this.startAccomplishment)/100;
      States.nbDiscovered = this.progress;
      States.nbInfos = this.max;

      switch (type) {
        case "zeroStep":
          this.textPop = d.popUp[0].text;
          this.popType = d.popUp[0].type;
          break;
        case "firstStep":
          this.textPop = d.popUp[1].text;
          this.popType = d.popUp[1].type;
          this.popUpChoiceBtnConfirm.innerHTML = d.popUp[1].btnConfirmTxt;
          this.popUpChoiceBtnCancel.innerHTML = d.popUp[1].btnCancelTxt;
          this.popUpChoiceTitle.innerHTML = d.popUp[1].title;
          States.nuclearfinished = true;
          localStorage.setItem('nuclearfinished',JSON.stringify(States.nuclearfinished));
          break;
        case "halfStep":
          this.textPop = dpop.popUp[2].text;
          this.popType = dpop.popUp[2].type;
          this.popUpChoiceBtnConfirm.innerHTML = dpop.popUp[2].btnConfirmTxt;
          this.popUpChoiceBtnCancel.innerHTML = dpop.popUp[2].btnCancelTxt;
          this.popUpChoiceTitle.innerHTML = dpop.popUp[2].title;
          break;
        case "missionDone":
          this.text = dpop.popUp[3].text;
          this.popType = dpop.popUp[3].type;
          break;
      }
      this.userProgress = userProgress;
      this.webgl.isShowingInfo = true;
      this.tlShowPopUpChoices.restart();
    },
    hidePopUpChoices(){
      this.webgl.isShowingInfo = false;
    }

  },

  transitions: {

  },

  components: {
    'xp-progress-component': XpProgressComponent,
    'pop-up-component': PopUpComponent,
    'pop-up-confirmation': PopUpConfirmationComponent,
    'loader-progress-component': LoaderProgressComponent,
    'loader-interaction-component': LoaderInteractionComponent,
    'footer-component': FooterComponent
  }
});
