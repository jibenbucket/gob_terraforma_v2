'use strict';

import './styles.scss';

import Emitter from 'core/Emitter';
import terra0 from 'data/terra-0.json';
import terra1 from 'data/terra-1.json';
import terra2 from 'data/terra-2.json';
import terra3 from 'data/terra-3.json';
import terra4 from 'data/terra-4.json';
import terra5 from 'data/terra-5.json';
import Spritesheet from 'utils/Spritesheet';

import {
  WINDOW_RESIZE
} from 'config/messages';

import SliderComponent from 'components/SlideToStart';
import TimelineComponent from 'components/Timeline';
import DynBgImgComponent from 'components/DynScrBgImg';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      progress: 0
    };
  },

  created() {

    this.dragStart = false;
    this.bind();
  },

  ready() {

    this.canvas = this.$els.canvas;
    this.spritesheet = new Spritesheet([
      terra0,
      terra1,
      terra2,
      terra3,
      terra4,
      terra5
    ], {
      width: 350,
      height: 350,
      path: '/images/sprites/terra/',
      fps: 60,
      autoStyle : false,
      retina: false,
      container : null,
      canvas: this.canvas,
      // loop: true,
      autoplay : false,
      onLoad : () => {},
      onComplete : () => {}
    });

    this.addEventListeners();

  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
    * Binding & Events
    */

    bind() {
    },

    addEventListeners() {
      Emitter.on(WINDOW_RESIZE, this.onWindowResize);
    },

    removeEventListeners() {
      Emitter.off(WINDOW_RESIZE, this.onWindowResize);
    },

    onWindowResize() {
      //resize components
    },

    onDragProgress( progressVal ) {
      if (!this.dragStart) {
        this.dragStart = true;
        this.canvas.style.backgroundImage = 'none';
      }
      this.spritesheet.goTo({ progress: progressVal });
      this.progress = progressVal;
    }
    // onWindowResize({width, height}) {
    //   console.log(`Window resize from application with debounce -> width: ${width}px || height: ${height}`);
    // },
  },

  transitions: {

  },

  components: {
    'slider-component': SliderComponent,
    'timeline-component': TimelineComponent,
    'dyn-bg-img-component': DynBgImgComponent
  }
});
