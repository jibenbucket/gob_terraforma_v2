'use strict';

import './styles.scss';
import TweenMax from 'gsap';
import Dragdealer from 'dragdealer';
import Router from 'core/Router';
import States from 'core/States';
import SoundManager from 'core/SoundManager';
import scrambleText from 'utils/ScrambleTextPlugin';
import data from 'data/datas-settings';

import FooterComponent from 'components/Footer';

import {
  WINDOW_RESIZE
} from 'config/messages';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      progress: 0
    };
  },

  created() {

    this.dragStart = false;
    this.previousX = 0;
  },

  ready() {

    this.earningTxt = this.$els.earningTxt;
    this.earningTop = this.$els.earningTop;
    this.earnedMoneyContainer = this.$els.earnedMoneyContainer;
    this.moneyAmontEl = this.$els.moneyAmont;
    this.moneyAmontValue = 0;
    this.title = this.$els.title;


    this.ressourceSeedContainer = this.$els.ressourceSeedContainer;
    this.ressourceOxygenContainer = this.$els.ressourceOxygenContainer;
    this.ressourceHydrogenContainer = this.$els.ressourceHydrogenContainer;
    this.dragSeedContainer = this.$els.dragSeedContainer;
    this.dragOxyContainer = this.$els.dragOxyContainer;
    this.dragHydroContainer = this.$els.dragHydroContainer;

    this.startNaturaBtn = this.$els.startNaturaBtn;
    this.startNaturaTxt = this.$els.startNaturaTxt;

    this.nbDiscoveredSpan = document.getElementById('nbDiscovered');
    this.nbInfosSpan = document.getElementById('nbInfos');
    this.nbDiscoveredSpan.innerHTML = States.nbDiscovered;
    this.nbInfosSpan.innerHTML = States.nbInfos;

    this.errorSpan = document.getElementById('natura-error');
    this.soundManager = SoundManager.getInstance();

    this.dragValues = [this.$els.seedDragValue,this.$els.oxyDragValue,this.$els.hydroDragValue];
    this.dragBars = [document.getElementById('current-seed-progress-bar'),document.getElementById('current-oxy-progress-bar'),document.getElementById('current-hydro-progress-bar')];

    this.percentTotal = 0;
    this.values = [0,0,0];

    this.generateTimelines();

    this.addEventListeners();

    this.initDrag();

    this.initializeValues();
  },

  beforeDestroy() {
  },

  methods: {
    addEventListeners() {
      this.startNaturaBtn.addEventListener( 'mouseup', this.startRenaturalization, false );
    },

    initDrag() {

      this.dragSeed = new Dragdealer(this.dragSeedContainer, {
        right: -25,
        x:0,
        y:0,
        animationCallback: (x, y)=> {
          this.updateValue(0,x);
        },

        dragStopCallback: (x, y)=> {
          this.isLastDrag();
        }
      });

      this.dragOxy = new Dragdealer(this.dragOxyContainer, {
        right: -25,
        x:0,
        y:0,
        animationCallback: (x, y)=> {
          this.updateValue(1,x);
        },
        dragStopCallback: (x, y)=> {
          this.isLastDrag();
        }
      });

      this.dragHydro = new Dragdealer(this.dragHydroContainer, {
        right: -25,
        x:0,
        y:0,
        animationCallback: (x, y)=> {
          this.updateValue(2,x);
        },
        dragStopCallback: (x, y)=> {
          this.isLastDrag();
        }
      });

    },

    updateValue(index,x) {
      this.values[index] = Math.round(x * 100);
      this.dragValues[index].innerHTML = this.values[index];
      this.percentTotal = 0;
      for (let i = 0; i < this.values.length; i++) {
        this.percentTotal += this.values[i];
      }
      this.moneyAmontEl.innerHTML = Math.round(this.moneyAmontValue * (100-this.percentTotal)/100);
      this.dragBars[index].style.width = 200*(this.values[index]/100)+"px";
      if (this.percentTotal < 90) {
        this.moneyAmontEl.style.color = '#7F7F7F';
      } else if (this.percentTotal <= 100) {
        this.moneyAmontEl.style.color = '#c0f0eb';
      } else {
        this.moneyAmontEl.style.color = '#ac3c34';
      }
    },

    initializeValues() {
      // this.updateValue(0,0.33)
      // this.updateValue(1,0.33)
      // this.updateValue(2,0.33)
      // this.isLastDrag();
    },

    isLastDrag(){
      if (this.percentTotal <= 90) {
        this.errorSpan.innerHTML = "You must spend at least  90% of your resources";
        this.startNaturaTxt.style.color = '#7F7F7F';
        // document.getElementById('resourceok').play();
      } else if (this.percentTotal > 100) {
        this.startNaturaTxt.style.color = '#7F7F7F';
        this.errorSpan.innerHTML = "You spent too much money";
      } else {
        this.startNaturaTxt.style.color = '#c0f0eb';
        this.errorSpan.innerHTML = "";
        TweenMax.to(this.startNaturaTxt, 1.5, { scrambleText: { text: "START RENATURALIZATION", chars: "abcdefghijklmnopqrstuvwxyz-°(){}&@_", speed: 1}}, "-=0.1");
      }
    },

    startRenaturalization() {
      if (this.percentTotal > 90 && this.percentTotal <= 100){
        localStorage.totalMoney = parseFloat(localStorage.totalMoney) + States.currentMoney;
        if(parseFloat(localStorage.oxyrate) > 0){
          localStorage.oxyrate = Number((parseFloat(localStorage.oxyrate) + this.values[0]*this.moneyAmontValue/data["maximumMoney"]-0.1).toFixed(3));
        } else {
          localStorage.oxyrate = Number((this.values[0]*this.moneyAmontValue/data["maximumMoney"]-0.1).toFixed(3));
        }
        if(parseFloat(localStorage.hydrorate)  > 0){
          localStorage.hydrorate = Number((parseFloat(localStorage.hydrorate) + this.values[1]*this.moneyAmontValue/data["maximumMoney"]-0.1).toFixed(2));
        } else {
          localStorage.hydrorate = Number((this.values[0]*this.moneyAmontValue/data["maximumMoney"]-0.1).toFixed(2));
        }
        Router.go({path:'/panorama'});
      }
    },

    generateTimelines() {
      this.tlEarnings = new TimelineMax( { paused: false });
      this.moneyAmontValue = States.currentMoney;
      this.earnedMoneyContainerYVal = (window.innerHeight / 2) - 100;
      this.earnedMoneyContainerXVal = ((window.innerWidth / 2) * 3) / 10;
      this.tlEarnings
      .to(this.earningTxt, 1, { y: "-70%", opacity: 1, ease: Power2.easeInOut, force3D:true , onComplete:()=> {this.soundManager.moneygain.play();}})//
      .to([ this.earningTop, this.earnedMoneyContainer ], 0.5, { opacity: 1, ease: Power2.easeInOut, force3D:true }, "-=0.4")
      .to(this.moneyAmontEl, 1.5, { scrambleText: { text: this.moneyAmontValue.toString(), chars: "1234567890", speed: 1}, onComplete:()=> {
        // TweenMax.to(this.moneyAmontEl, 1.5, { scrambleText: { text: "0", chars: "1234567890", speed: 1}, delay:3});
        let sliderValue = {value:0};
        TweenMax.to(sliderValue,1,{ease:Linear.easeIn, value:0.333333333, delay:3, onUpdate:()=>{
          this.updateValue(1,sliderValue.value)
          this.updateValue(1,sliderValue.value)
          this.updateValue(2,sliderValue.value)
          this.dragSeed.setValue(sliderValue.value,0,false);
          this.dragOxy.setValue(sliderValue.value,0,false);
          this.dragHydro.setValue(sliderValue.value,0,false);

        }, onComplete:()=>{
          this.isLastDrag();
        }});
      }}, "-=0.1")
      .to(this.earningTxt, 1, { y: "70%", opacity: 0, ease: Power2.easeInOut, force3D:true, onComplete:()=> {this.soundManager.transitionmoney.play();}})//
      .to([ this.earningTop, this.earnedMoneyContainer ], 1, { x: this.earnedMoneyContainerXVal+"%", y:"-"+this.earnedMoneyContainerYVal, scale:0.8, ease: Power2.easeInOut, force3D:true , onComplete:()=> {this.soundManager.revealresources.play();}})//
      .to([this.title, this.ressourceSeedContainer, this.ressourceOxygenContainer, this.ressourceHydrogenContainer, this.startNaturaBtn], 1, { y: "-70%", opacity: 1, ease: Power2.easeInOut, force3D:true });

      this.tlEarnings.play();
    }
  },

  transitions: {

  },

  components: {
    'footer-component': FooterComponent
    // 'timeline-component': TimelineComponent,
    // 'dyn-bg-img-component': DynBgImgComponent
  }
});
