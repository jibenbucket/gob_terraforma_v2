'use strict';

import './styles.scss';
import Emitter from 'core/Emitter'

import States from 'core/States'

import FooterComponent from 'components/Footer';

import {
  WINDOW_RESIZE
} from 'config/messages';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      isDesktop: false
    };
  },

  created() {
    this.bind();
    this.isDesktop = States.getDeviceType() == "desktop"
  },

  ready() {
    this.addEventListeners();

  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
    * Binding & Events
    */

    bind() {
    },

    addEventListeners() {
      Emitter.on(WINDOW_RESIZE, this.onWindowResize);
    },

    removeEventListeners() {
      Emitter.off(WINDOW_RESIZE, this.onWindowResize);
    },

    onWindowResize() {
      //resize components
    },

    onDragProgress( progressVal ) {
    }
  },

  transitions: {

  },

  components: {
    'footer-component': FooterComponent
  }
});
