'use strict';

import './styles.scss';
import Emitter from 'core/Emitter'

import States from 'core/States'
import scrambleText from 'utils/ScrambleTextPlugin';

import FooterComponent from 'components/Footer';

import {
  WINDOW_RESIZE
} from 'config/messages';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
    };
  },

  created() {
    this.bind();
  },

  ready() {
    this.addEventListeners();
    this.titleCongrats = this.$els.titleCongrats
    console.log(this.titleCongrats);
    this.tlReady = new TimelineMax( { paused: false });
    this.tlReady
    .to(this.$els.titleSeparator, 1, {
      width: "100%",
      ease: Power2.easeInOut,
      force3D:true
    })
    .to(this.titleCongrats, 1, { scrambleText: { text: "congratulations", chars: "abcdefghijklmnopqrstuvwxyz-°(){}&@_", speed: 1}},"-=0.7")//
    this.tlReady.play()

  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
    * Binding & Events
    */

    bind() {
    },

    addEventListeners() {
      Emitter.on(WINDOW_RESIZE, this.onWindowResize);
    },

    removeEventListeners() {
      Emitter.off(WINDOW_RESIZE, this.onWindowResize);
    },

    onWindowResize() {
      //resize components
    },

    onDragProgress( progressVal ) {
    }
  },

  transitions: {

  },

  components: {
    'footer-component': FooterComponent
  }
});
