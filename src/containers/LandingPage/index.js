'use strict';

import './styles.scss';

import Emitter from 'core/Emitter';
import terra0 from 'data/terra-0.json';
import terra1 from 'data/terra-1.json';
import terra2 from 'data/terra-2.json';
import terra3 from 'data/terra-3.json';
import terra4 from 'data/terra-4.json';
import terra5 from 'data/terra-5.json';
import Spritesheet from 'utils/Spritesheet';
import SoundManager from 'core/SoundManager';

import {
  WINDOW_RESIZE
} from 'config/messages';

import SliderComponent from 'components/SlideToStart';
import TimelineComponent from 'components/Timeline';
import DynBgImgComponent from 'components/DynScrBgImg';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      progress: 0
    };
  },

  created() {

    this.dragStart = false;
    this.bind();
    this.redirectIfMobile("terraforma.xyz");
  },

  ready() {

    this.canvas = this.$els.canvas;
    this.spritesheet = new Spritesheet([
      terra0,
      terra1,
      terra2,
      terra3,
      terra4,
      terra5
    ], {
      width: 350,
      height: 350,
      path: '/images/sprites/terra/',
      fps: 60,
      autoStyle : false,
      retina: false,
      container : null,
      canvas: this.canvas,
      // loop: true,
      autoplay : false,
      onLoad : () => {},
      onComplete : () => {}
    });

    this.addEventListeners();
    window.onblur = ()=> {
      SoundManager.mute();
    };
    window.onfocus = ()=> {
      SoundManager.demute();
    };
    SoundManager.demute();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
    * Binding & Events
    */

    bind() {
    },

    addEventListeners() {
      Emitter.on(WINDOW_RESIZE, this.onWindowResize);
    },

    removeEventListeners() {
      Emitter.off(WINDOW_RESIZE, this.onWindowResize);
    },

    onWindowResize() {
      //resize components
    },

    onDragProgress( progressVal ) {
      if (!this.dragStart) {
        this.dragStart = true;
        this.canvas.style.backgroundImage = 'none';
      }
      this.spritesheet.goTo({ progress: progressVal });
      this.progress = progressVal;
    },
    // onWindowResize({width, height}) {
    //   console.log(`Window resize from application with debounce -> width: ${width}px || height: ${height}`);
    // },

    redirectIfMobile( url ) {
      (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))window.location=b})(navigator.userAgent||navigator.vendor||window.opera,url);
    }
  },

  transitions: {

  },

  components: {
    'slider-component': SliderComponent,
    'timeline-component': TimelineComponent,
    'dyn-bg-img-component': DynBgImgComponent
  }
});
