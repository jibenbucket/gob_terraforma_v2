'use strict';

import './styles.scss';
import Router from 'core/Router';
import States from 'core/States';
import Emitter from 'core/Emitter';

import SoundManager from 'core/SoundManager';

import NaturalizationProgressComponent from 'components/NaturalizationProgressBar';
import LoaderProgressComponent from 'components/LoaderProgressBar';
import LoaderInteractionComponent from 'components/LoaderInteractionPanorama';
import FooterComponent from 'components/Footer';

import {
  WINDOW_RESIZE
} from 'config/messages';

import THREE from 'three';
import TweenMax from 'gsap';

import Webgl from 'core/webgl/PanoramaWebgl';
import raf from 'raf';
import 'gsap';

let raycaster = new THREE.Raycaster();
let mouse = new THREE.Vector2();

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      loaderprogress: 0.1,
      naturalizationprogress: 0
    };
  },

  created() {

    this.mouseConfig = {
      isUserInteracting : false,
      onMouseDownMouseX : 0,
      onMouseDownMouseY : 0,
      onPointerDownPointerX : 0,
      onPointerDownPointerY : 0,
      onPointerDownLon : 0,
      onPointerDownLat : 0,
      lat : 0,
      lon : 0,
      phi : 0,
      theta : 0
    };
    this.distance = 500;
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    //webgl settings
    this.webgl = new Webgl(window.innerWidth, window.innerHeight);

    this.bind();

    this.loaderprogress = 0;
    // this.naturalizationRate = parseFloat(localStorage.naturalizationRate);
    this.start = false;
    this.isReady = false;
  },

  ready() {

    let three = document.getElementById('three-container');
    this.loader = document.getElementById('loader-container');
    this.startButton = document.getElementById('progress-start');
    this.startButton.addEventListener('click', this.onStartButton, false);
    this.infoPano = this.$els.infoPano;
    this.oxyspan = document.getElementById('oxyrate');
    this.hydrospan = document.getElementById('hydrorate');
    // this.terraspan = document.getElementById('terrarate');
    this.locationspan = document.getElementById('location');
    this.datespan = document.getElementById('date');
    this.oxyspan.innerHTML = localStorage.oxyrate;
    this.hydrospan.innerHTML = localStorage.hydrorate;
    // this.terraspan.innerHTML = this.naturalizationprogress*100;
    let currentDate = new Date();
    this.datespan.innerHTML = currentDate.getDate() + "." + (currentDate.getMonth()-1);
    this.locationspan.innerHTML = "EARTH";

    // this.locationspan.innerHTML = location;
    three.appendChild(this.webgl.renderer.domElement);
    this.webgl.renderer.domElement.style.cursor = "move";
    this.animate();
    window.onblur = ()=> {
      SoundManager.mute();
      this.webgl.pauseRender();
    };
    window.onfocus = ()=> {
      SoundManager.demute();
      this.webgl.resumeRender();
    };
    SoundManager.demute();
    this.webgl.onReady();

    this.loaderprogress = Math.random()*0.8;
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
    * Binding & Events
    */

    bind() {
      this.animate = this.animate.bind(this);
    },

    addEventListeners() {
      window.onblur = ()=> {
        this.webgl.pauseRender();
      };
      window.onfocus = ()=> {
        this.webgl.resumeRender();
      };
      Emitter.on(WINDOW_RESIZE, this.onWindowResize);
      document.addEventListener( 'mousedown', this.onDocumentMouseDown, false );
      document.addEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.addEventListener( 'mouseup', this.onDocumentMouseUp, false );
      document.addEventListener( 'mousewheel', this.onDocumentMouseWheel, false );
      document.addEventListener( 'MozMousePixelScroll', this.onDocumentMouseWheel, false);
      document.addEventListener('keypress', this.onKeyPress, false);
    },

    removeEventListeners() {
      Emitter.off(WINDOW_RESIZE, this.onWindowResize);
      document.removeEventListener( 'mousedown', this.onDocumentMouseDown, false );
      document.removeEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.removeEventListener( 'mouseup', this.onDocumentMouseUp, false );
      document.removeEventListener( 'mousewheel', this.onDocumentMouseWheel, false );
      document.removeEventListener( 'MozMousePixelScroll', this.onDocumentMouseWheel, false);
      document.removeEventListener('keypress', this.onKeyPress, false);
    },

    onWindowResize() {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.webgl.resize(this.width, this.height);
    },

    animate() {
      raf(this.animate);
      this.updateMouse();
      this.webgl.render();
      if(this.webgl.loaderProgress == 1) {
        if(!this.isReady) {
          this.isReady = true;
        // if(this.start == false) {
          // if(this.webgl.isReady) {
            // this.loaderprogress = this.webgl.panorama.loaderprogress;
          this.loaderprogress = 1;//this.webgl.loaderProgress;
            // TweenMax.to(this.startButton,0.3,{opacity:1});

          // }
        // }
        }
        if(this.webgl.videoEl.video.readyState == 2) {
          //show loader
        }
      }
    },

    updateMouse(){
      if(this.naturalizationprogress != parseFloat(localStorage.naturalizationRate)) {
        this.naturalizationprogress = parseFloat(localStorage.naturalizationRate);
      }

      if(!this.webgl.isGoingOnExp) {
        this.mouseConfig.lat = Math.max( - 85, Math.min( 85, this.mouseConfig.lat ) );
        this.mouseConfig.phi = THREE.Math.degToRad( 90 - this.mouseConfig.lat );
        this.mouseConfig.theta = THREE.Math.degToRad( this.mouseConfig.lon );
        this.webgl.camera.target.x = 500 * Math.sin( this.mouseConfig.phi ) * Math.cos( this.mouseConfig.theta );
        this.webgl.camera.target.y = 500 * Math.cos( this.mouseConfig.phi );
        this.webgl.camera.target.z = 500 * Math.sin( this.mouseConfig.phi ) * Math.sin( this.mouseConfig.theta );
        this.webgl.camera.lookAt( this.webgl.camera.target );
      }
    },

    onStartButton ( event ) {
        this.webgl.render();
        TweenMax.to(this.loader,0.3,{opacity:0,onComplete:()=>{
          this.start = true;
           this.webgl.videoEl.video.play();
          TweenMax.set(this.loader,{display:'none'});
        }});
        this.startButton.removeEventListener('click', this.onStartButton, false);
        TweenMax.set(this.infoPano, { display: "block" });
        this.addEventListeners();
    },

    onDocumentMouseDown( event ) {
      event.preventDefault();
      // this.mouseConfig.isUserInteracting = !this.mouseConfig.isUserInteracting;
      this.mouseConfig.isUserInteracting = true;
      this.mouseConfig.onPointerDownPointerX = event.clientX;
      this.mouseConfig.onPointerDownPointerY = event.clientY;
      this.mouseConfig.onPointerDownLon = this.mouseConfig.lon;
      this.mouseConfig.onPointerDownLat = this.mouseConfig.lat;
    },

    onDocumentMouseUp( event ) {
      this.mouseConfig.isUserInteracting = false;
      mouse.set(( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1);
      this.raycast(true);
    },

    onDocumentMouseMove( event ) {
      if ( this.mouseConfig.isUserInteracting === true ) {
        this.mouseConfig.lon = ( this.mouseConfig.onPointerDownPointerX - event.clientX ) * 0.1 + this.mouseConfig.onPointerDownLon;
        this.mouseConfig.lat = ( event.clientY - this.mouseConfig.onPointerDownPointerY ) * 0.1 + this.mouseConfig.onPointerDownLat;
      }
      mouse.set(( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1);
      this.raycast(false);
    },

    onDocumentMouseWheel( event ) {
      // WebKit
      // if ( event.wheelDeltaY ) {
      //   this.webgl.camera.fov -= event.wheelDeltaY * 0.05;
      // // Opera / Explorer 9
      // } else if ( event.wheelDelta ) {
      //   this.webgl.camera.fov -= event.wheelDelta * 0.05;
      // // Firefox
      // } else if ( event.detail ) {
      //   this.webgl.camera.fov += event.detail * 1.0;
      // }
      // this.webgl.camera.updateProjectionMatrix();
    },
    onKeyPress(event) {
      this.webgl.addGlitch();
      // switch(event.keyCode) {
      //   case 97:
      //     // this.webgl.addFace(0);
      //     break;
      // }
    },
    raycast(clicked) {
      raycaster.setFromCamera( mouse, this.webgl.camera );

      // calculate objects intersecting the picking ray
      let intersects = raycaster.intersectObjects( this.webgl.scene.children, true );
      var insterctsTarget = false;
      for ( let i = 0; i < intersects.length; i++ ) {
        if (intersects[ i ].object.name == 'pesticides') {
          insterctsTarget = true;
          if(clicked) {
            this.webgl.animateTo(0,'pesticides');
          } else {
            // change cursor
            this.webgl.renderer.domElement.style.cursor = "pointer";
          }
        } else if (intersects[ i ].object.name == 'nuclear') {
          insterctsTarget = true;
          if(clicked) {
            this.webgl.animateTo(1,'nuclear');
          }else{
            this.webgl.renderer.domElement.style.cursor = "pointer";
          }
        }
      }
      if(!insterctsTarget) {
        this.webgl.renderer.domElement.style.cursor = "move";
      }
    }
  },

  transitions: {

  },

  components: {
    'naturalization-progress-component': NaturalizationProgressComponent,
    'loader-progress-component': LoaderProgressComponent,
    'loader-interaction-component': LoaderInteractionComponent,
    'footer-component': FooterComponent
  }
});
