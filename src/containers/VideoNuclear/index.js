'use strict';

import './styles.scss';
import Router from 'core/Router';
import SoundManager from 'core/SoundManager';

import data from 'data/sub-nuclear.json';

import Subtitle from 'utils/Subtitles';

import Emitter from 'core/Emitter';
import FooterComponent from 'components/Footer';
import SubComponent from 'components/Subtitle';

import {
  WINDOW_RESIZE
} from 'config/messages';

import THREE from 'three';

import Webgl from 'core/webgl/VideoWebgl';
import raf from 'raf';
let raycaster = new THREE.Raycaster();
let mouse = new THREE.Vector2();

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      _hidden: null
    };
  },

  created() {

    this.mouseConfig = {
      onMouseDownMouseX : 0,
      onMouseDownMouseY : 0,
      onPointerDownLon : 0,
      onPointerDownLat : 0,
      lat : 0,
      lon : 0,
      phi : 0,
      theta : 0
    };
    this.distance = 500;
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    //webgl settings
    this.webgl = new Webgl('nuclear.mp4');
    this.isRendering = true;
    this.loadingShown = true;

    this.bind();
  },

  ready() {

    let three = document.getElementById('three-container');
    three.appendChild(this.webgl.renderer.domElement);
    this.progressBar = this.$els.progressBar;
    this.timeMinutes = this.$els.timeMinutes;
    this.timeSeconds = this.$els.timeSeconds;
    this.sub = new Subtitle({timeline: data.timeline, player: this.webgl.videoEl.video });
    this.loadingPopup = document.getElementById("video-loader");
    this.pausePopup = document.getElementById("video-pause");

    this.playerInfos = this.$els.videoPlayerInfos;

    this.addEventListeners();
    this.animate();
    window.onblur = ()=> {
      this.pauseVideo();
    };
    window.onfocus = ()=> {
      this.playVideo();
    };
    SoundManager.mute();

    Emitter.on('MUTE', ()=>{
      this.webgl.videoEl.video.muted = true;
    })
    Emitter.on('DEMUTE', ()=>{
      this.webgl.videoEl.video.muted = false;
    })

  },

  beforeDestroy() {
    this.stopVideo();
    this.sub.destroy();
    this.removeEventListeners();
  },

  methods: {

    /*
    * Binding & Events
    */

    bind() {
      this.animate = this.animate.bind(this);
    },

    playVideo(){
      if (this.webgl.videoEl.video.paused){
        this.webgl.videoEl.video.play();
        this.pausePopup.style.display = "none";
      }
    },
    pauseVideo(){
      if (!this.webgl.videoEl.video.paused){
        this.webgl.videoEl.video.pause();
        this.pausePopup.style.display = "block";
      }
    },
    stopVideo(){
      this.webgl.videoEl.video.pause();
      this.pausePopup.style.display = "none";
      this.webgl.videoEl.video.currentTime = 0;
     },

    addEventListeners() {
      Emitter.on(WINDOW_RESIZE, this.onWindowResize);
      document.addEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.addEventListener('keypress', this.onKeyPressed, false);
      this.webgl.videoEl.video.addEventListener('timeupdate', this.updateProgressBar, false);
      this.webgl.videoEl.video.addEventListener('ended', this.endVideo, false);
      this.playerInfos.addEventListener('click',this.skipAhead, false);
    },

    removeEventListeners() {
      Emitter.off(WINDOW_RESIZE, this.onWindowResize);
      document.removeEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.removeEventListener('keypress', this.onKeyPressed, false);
    },

    onWindowResize() {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.webgl.resize(this.width, this.height);
    },

    animate() {
      if(this.isRendering) {
        raf(this.animate);

        this.update();

        this.webgl.render();
        if (this.webgl.videoEl.video.readyState <= 1) {
          if(!this.loadingShown) {
            this.showLoading();
            this.loadingShown = true;
          }
        } else {
          if(this.loadingShown) {
            this.hideLoading();
            this.loadingShown = false;
          }
        }
      }
    },

    showLoading() {
      if(this.loadingPopup !== null) {
        TweenMax.to(this.loadingPopup, 0.2,{opacity: 1, onComplete:()=>{
            this.webgl.videoEl.video.pause();
            this.loadingPopup.style.display = "block";
          }
        });
      }
    },

    hideLoading() {
      if(this.loadingPopup !== null) {
        TweenMax.to(this.loadingPopup, 0.2,{opacity: 0, onComplete:()=>{
          this.webgl.videoEl.video.play();
          this.loadingPopup.style.display = "none";
        }
      });
      }
    },

    onDocumentMouseMove( event ) {
      // if ( this.mouseConfig.isUserInteracting === true ) {
      this.mouseConfig.onPointerDownPointerX = event.clientX;
      this.mouseConfig.onPointerDownPointerY = event.clientY;
      this.mouseConfig.onPointerDownLon = this.mouseConfig.lon;
      this.mouseConfig.onPointerDownLat = this.mouseConfig.lat;

      this.mouseConfig.lon = ( event.clientX / this.width ) * 430 - 225;
      this.mouseConfig.lat = ( (this.height-event.clientY) / this.height ) * -180 + 90;

        // this.mouseConfig.lon = ( this.mouseConfig.onPointerDownPointerX - event.clientX ) * 0.1 + this.mouseConfig.onPointerDownLon;
        // this.mouseConfig.lat = ( event.clientY - this.mouseConfig.onPointerDownPointerY ) * 0.1 + this.mouseConfig.onPointerDownLat;
      // }
    },
    onKeyPressed( event ) {
      if (event.code == "Space") {
        if (!this.webgl.videoEl.video.paused){
          this.pauseVideo();
        } else {
          this.playVideo();
        }
      }
    },
    updateProgressBar() {
      let seconds;
      let percentage = Math.floor((100 / this.webgl.videoEl.video.duration) * this.webgl.videoEl.video.currentTime);
      this.progressBar.value = percentage;
      let currentMinutes = parseInt(this.webgl.videoEl.video.currentTime / 60);
      let durationMinutes = parseInt(this.webgl.videoEl.video.duration / 60);
      let diffMinutes;// = (durationMinutes - currentMinutes) >= 1 ? (durationMinutes - currentMinutes) : 0 ;
      if ((this.webgl.videoEl.video.duration - this.webgl.videoEl.video.currentTime) > 60) {
        seconds = parseInt(this.webgl.videoEl.video.duration - this.webgl.videoEl.video.currentTime) - 59 * durationMinutes;
        diffMinutes = 1;
      } else {
        seconds = parseInt(this.webgl.videoEl.video.duration - this.webgl.videoEl.video.currentTime);
        diffMinutes = 0;
      }
      this.timeMinutes.innerHTML = "0"+diffMinutes;
      this.timeSeconds.innerHTML = seconds < 10 ? "0"+seconds : seconds;

    },
    skipAhead(e) {
      if(e.target.className == "shortcut-info-pause") {
        if (!this.webgl.videoEl.video.paused){
          this.pauseVideo();
        } else {
          this.playVideo();
        }
        return;
      } else {
          let pos = (e.pageX  - this.progressBar.getBoundingClientRect().x) / this.progressBar.getBoundingClientRect().width;
          this.webgl.videoEl.video.currentTime = pos * this.webgl.videoEl.video.duration;
          this.pausePopup.style.display = "none";
          this.sub.update();
      }
    },
    endVideo() {
      this.isRendering = false;
      Router.go({
        name: 'xp-nuclear'
      });
    },

    onSkipBtn() {
      this.webgl.videoEl.video.pause();
      this.endVideo();
    },
    update(){
      this.mouseConfig.lat = Math.max( - 85, Math.min( 85, this.mouseConfig.lat ) );
			this.mouseConfig.phi = THREE.Math.degToRad( 90 - this.mouseConfig.lat );
			this.mouseConfig.theta = THREE.Math.degToRad( this.mouseConfig.lon/3 );
			this.webgl.camera.position.x = this.distance * Math.sin( this.mouseConfig.phi ) * Math.cos( this.mouseConfig.theta );
			this.webgl.camera.position.y = this.distance * Math.cos( this.mouseConfig.phi );
			this.webgl.camera.position.z = this.distance * Math.sin( this.mouseConfig.phi ) * Math.sin( this.mouseConfig.theta );
			this.webgl.camera.lookAt( this.webgl.camera.target );
    }

  },

  transitions: {

  },

  components: {
    'subtitle-component': SubComponent,
    'footer-component': FooterComponent
  }
});
