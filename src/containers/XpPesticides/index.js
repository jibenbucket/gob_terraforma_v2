'use strict';

import './styles.scss';

import Emitter from 'core/Emitter';
import Router from 'core/Router';
import {
  WINDOW_RESIZE,
  HIDE_INFOS_POP,
  SHOW_INFOS_POP,
  HIDE_POPUP_CHOICES
} from 'config/messages';

import THREE from 'three';

import XpProgressComponent from 'components/XpProgressBar';
import PopUpComponent from 'components/PopUpPesticides';
import LoaderProgressComponent from 'components/LoaderProgressBar';
import PopUpConfirmationComponent from 'components/PopUpConfirmation';
import LoaderInteractionComponent from 'components/LoaderInteractionPesticides';
import LevelIndicatorComponent from 'components/LevelIndicator';
import FooterComponent from 'components/Footer';
import States from 'core/States';
import SoundManager from 'core/SoundManager';

import Webgl from 'core/webgl/XpPesticidesWebgl';
import d from 'data/datas-xp-pesticides';
import dpop from 'data/datas-xp-nuclear';
import raf from 'raf';
import 'gsap';

let raycaster = new THREE.Raycaster();
let mouse = new THREE.Vector2();
let aDelete = 0;
let MAXIMUM_MONEY = d['maximumMoney'];
let seedStartSeconds = 1000;

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      loaderprogress: 0,
      progress: 0,
      step    : 3,
      max     : 0,
      accomplishment:0,
      levelindex : 0,
      nblevels: 0,
      title: "Titre",
      image: "Helloooooo",
      text: "Le texte",
      textPop: "",
      popType: ""
    };
  },

  created() {

    this.mouseConfig = {
      isUserInteracting : false,
      onMouseDownMouseX : 0,
      onMouseDownMouseY : 0,
      onPointerDownPointerX : 0,
      onPointerDownPointerY : 0,
      onPointerDownLon : 0,
      onPointerDownLat : 0,
      lat : 0,
      lon : 0,
      phi : 0,
      theta : 0
    };

    //webgl settings
    this.webgl = new Webgl(window.innerWidth, window.innerHeight);
    this.loaderprogress = 0;
    this.max = this.webgl.getMax();
    this.start = false;
    this.levelindex = 0;
    this.nblevels = this.webgl.levels.length;

    this.bind();
  },

  ready() {
    this.threeContainer = document.getElementById("three-container");
    this.threeContainer.appendChild(this.webgl.renderer.domElement);
    this.loader = document.getElementById('loader-container');
    this.startButton = document.getElementById('progress-start');
    this.startButton.addEventListener('click', this.onStartButton, false);
    // this.onStartButton();
    this.imageEl = document.getElementById('image');
    this.popUpContainer = document.getElementById('pop-up-pesticide-container');
    this.popUpConfirm = document.getElementById('pop-up-confirm-container');
    this.popUpConfirmBg = document.getElementById('pop-up-background');
    this.popUpInfosBg = document.getElementById('pop-up-infos-pesticide-background');
    this.popUpChoiceBtnConfirm = document.getElementById('pop-btn-confirm');
    this.popUpChoiceBtnCancel = document.getElementById('pop-btn-cancel');
    this.popUpChoiceTitle = document.getElementById('pop-up-choice-title');
    this.generateTl();
    this.animate();
    this.progress = this.webgl.progress;

    this.soundManager = SoundManager.getInstance();
    SoundManager.demute();

  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
    * Binding & Events
    */

    bind() {
      this.animate = this.animate.bind(this);
    },

    generateTl() {
      this.tlShowPopUpChoices = new TimelineMax( { paused: true });
      this.tlShowPopUpChoices
      .to([this.popUpConfirmBg, this.popUpConfirm], 0.1, { display: "block", ease: Expo.easeIn })
      .fromTo([this.popUpConfirmBg, this.popUpConfirm], 0.5, { opacity: 0 } , { opacity : 0.94 }, Expo.easeIn, "-=0.1");

      this.tlShowPopInfos = new TimelineMax( { paused: true });
      this.tlShowPopInfos
      .to([this.popUpInfosBg, this.popUpContainer], 0.1, { display: "block", ease: Expo.easeIn })
      .fromTo([this.popUpInfosBg, this.popUpContainer], 0.5, { opacity: 0 } , { opacity : 0.94, onStart:()=>{
        // TweenMax.to(this.popUpInfoTitle, 1, { scrambleText: { text: this.title, chars: "abcdefghijklmnopqrstuvwxyz-°(){}&@_", speed: 1}}, "-=1");
      }}, Expo.easeIn);

      this.tlHidePopUpInfos = new TimelineMax( { paused: true });
      this.tlHidePopUpInfos
      .fromTo([this.popUpInfosBg, this.popUpContainer], 0.5, { opacity: 0.94 } , { opacity : 0 }, Expo.easeIn)
      .to([this.popUpInfosBg, this.popUpContainer], 0.1, { display: "none", ease: Expo.easeIn, onComplete:()=>{
        if (this.progress == this.step) {
          //show pop up first step
          this.showPopUpChoices("firstStep", this.webgl.progress);
        } else if (this.progress >= this.max/2) {
          this.showPopUpChoices("halfStep", this.webgl.progress);
        } else if (this.progress >= this.max) {
          this.showPopUpChoices("missionDone", this.webgl.progress);
        }
      }});
      // .fromTo(this.popUpContainer, 0.7, { scale: 1, x: "-50%", y:"-50%" }, { scale: 0, x: "-50%", y:"-50%", ease: Expo.easeIn, onComplete:()=>{
      //   if (this.progress == 1) {
      //     //show pop up first step
      //     this.showPopUpChoices("firstStep", this.webgl.progress);
      //
      //   }
      // }}, "-=0.1");

    },

    addEventListeners() {
      window.onblur = ()=> {
        this.webgl.pauseRender();
        SoundManager.mute();
      };
      window.onfocus = ()=> {
        this.webgl.resumeRender();
        SoundManager.demute();
      };
      Emitter.on(WINDOW_RESIZE, this.onWindowResize);
      Emitter.on(SHOW_INFOS_POP, this.showPopUpInfos);
      Emitter.on(HIDE_INFOS_POP, this.popDown);
      Emitter.on(HIDE_POPUP_CHOICES, this.hidePopUpChoices);
      document.addEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.addEventListener( 'mouseup', this.onDocumentMouseUp, false );
      document.addEventListener( 'mousewheel', this.onDocumentMouseWheel, false );
      document.addEventListener( 'MozMousePixelScroll', this.onDocumentMouseWheel, false);
      // document.addEventListener('keydown', this.onKeyDown, false);
    },

    removeEventListeners() {
      Emitter.off(WINDOW_RESIZE, this.onWindowResize);
      Emitter.off(SHOW_INFOS_POP, this.showPopUpInfos);
      Emitter.off(HIDE_INFOS_POP, this.popDown);
      Emitter.off(HIDE_POPUP_CHOICES, this.hidePopUpChoices);
      document.removeEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.removeEventListener( 'mouseup', this.onDocumentMouseUp, false );
      document.removeEventListener( 'mousewheel', this.onDocumentMouseWheel, false );
      document.removeEventListener( 'MozMousePixelScroll', this.onDocumentMouseWheel, false);
      // document.removeEventListener('keydown', this.onKeyDown, false);
    },

    onWindowResize() {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.webgl.resize(this.width, this.height);
    },

    animate() {
      raf(this.animate);
      this.accomplishment = Math.round(this.progress/this.max*100);

      if(this.start == false) {
        this.loaderprogress = this.webgl.getNbObjectsLoaded() / this.max;
        if (this.loaderprogress >= 1) {
          TweenMax.to(this.startButton,0.3,{opacity:1});
        }
      } else {
        // Pour les components
        this.mouseConfig.lat = Math.max( - 85, Math.min( 85, this.mouseConfig.lat ) );
        this.mouseConfig.phi = THREE.Math.degToRad( 90 - this.mouseConfig.lat );
        this.mouseConfig.theta = THREE.Math.degToRad( this.mouseConfig.lon );

        this.webgl.render();
        if(this.webgl.isLeveling)
          this.levelindex = this.webgl.levelIndex;
      }
    },

    onStartButton ( event ) {
        this.soundManager.pesticides.play();
        this.webgl.render();
        TweenMax.to(this.loader,0.3,{opacity:0,onComplete:()=>{
          this.start = true;
          TweenMax.set(this.loader,{display:'none'});
        }});
        this.startButton.removeEventListener('click', this.onStartButton, false);
        this.webgl.resize(window.innerWidth, window.innerHeight);
        this.webgl.setProgressFromLocalStorage();
        this.levelindex = this.webgl.levelIndex;
        this.progress = this.webgl.progress;
        this.startAccomplishment = Math.round(this.progress/this.max*100);
        window.setTimeout(()=>{
          this.webgl.isReady = true;
        },seedStartSeconds);
        this.addEventListeners();
    },

    popUp() {
      this.setInfoContent();
      TweenMax.set(this.threeContainer,{cursor:'default'});
      TweenMax.set(this.popUpContainer,{visibility:'visible',opacity:0});
      TweenMax.to(this.popUpContainer,1,{opacity:1});
    },
    showPopUpInfos() {
      TweenMax.set(this.threeContainer,{cursor:'default'});
      this.setInfoContent();
      this.imageEl.style.backgroundImage = "url("+this.image+")";
      this.tlShowPopInfos.restart();
      this.progress = this.webgl.progress;
    },

    showPopUpChoices(type, userProgress){
      States.currentMoney = MAXIMUM_MONEY * (this.accomplishment-this.startAccomplishment)/100;
      States.nbDiscovered = this.progress;
      States.nbInfos = this.max;

      switch (type) {
        case "zeroStep":
          this.textPop = dpop.popUp[0].text;
          this.popType = dpop.popUp[0].type;
          break;
        case "firstStep":
          this.textPop = dpop.popUp[1].text;
          this.popType = dpop.popUp[1].type;
          this.popUpChoiceBtnConfirm.innerHTML = dpop.popUp[1].btnConfirmTxt;
          this.popUpChoiceBtnCancel.innerHTML = dpop.popUp[1].btnCancelTxt;
          this.popUpChoiceTitle.innerHTML = dpop.popUp[1].title;
          States.pesticidesfinished = true;
          localStorage.setItem('pesticidesfinished',JSON.stringify(States.pesticidesfinished));
          break;
        case "halfStep":
          this.textPop = dpop.popUp[2].text;
          this.popType = dpop.popUp[2].type;
          this.popUpChoiceBtnConfirm.innerHTML = dpop.popUp[2].btnConfirmTxt;
          this.popUpChoiceBtnCancel.innerHTML = dpop.popUp[2].btnCancelTxt;
          this.popUpChoiceTitle.innerHTML = dpop.popUp[2].title;
          break;
        case "missionDone":
          this.text = dpop.popUp[3].text;
          this.popType = dpop.popUp[3].type;
          break;
      }
      this.userProgress = userProgress;
      this.webgl.isShowingInfo = true;
      this.tlShowPopUpChoices.restart();
    },
    hidePopUpChoices() {
      this.webgl.isShowingInfo = false;
    },

    goHomeBtn() {
      this.webgl.saveProgresToLocalStorage();
      States.currentMoney = MAXIMUM_MONEY * (this.accomplishment-this.startAccomplishment)/100;
      States.nbDiscovered = this.progress;
      States.nbInfos = this.max;

      if (this.webgl.progress < this.step){
        this.showPopUpChoices("zeroStep", this.webgl.progress);
        // this.webgl.isShowingInfo == true;
        // // display warning experience not finish
        // // this.textPop = "ARE YOU REALLY SURE ??";
        // this.tlShowPopUpChoices.restart();
      } else {
        States.pesticidesfinished = true;
        localStorage.setItem('pesticidesfinished',JSON.stringify(States.pesticidesfinished));
        Router.go({ name: 'buy-resources'});
      }
    },
    popDown() {
      this.webgl.resetCameraPosition();
      this.tlHidePopUpInfos.restart();
    },

    setInfoContent() {
      this.title = d['levels'][this.webgl.levelIndex]["objects"][this.webgl.selectedObjectIndex].name;
      this.image = d['levels'][this.webgl.levelIndex]["objects"][this.webgl.selectedObjectIndex].image;
      this.text = d['levels'][this.webgl.levelIndex]["objects"][this.webgl.selectedObjectIndex].descr;
    },

    onDocumentMouseUp ( event ) {
        this.webgl.onClick(event);
    },

    onDocumentMouseMove( event ) {
      this.webgl.onMouseMove(event);
    },

    onDocumentMouseWheel( event ) {
      if(!this.isShowingInfo) {
        event.preventDefault();
        // WebKit
        if ( event.wheelDeltaY ) {
          //this.webgl.camera.fov -= event.wheelDeltaY * 0.05;
          aDelete -= event.wheelDeltaY * 0.05;
        // Opera / Explorer 9
        } else if ( event.wheelDelta ) {
          //this.webgl.camera.fov -= event.wheelDelta * 0.05;
          aDelete -= event.wheelDelta * 0.05;
        // Firefox
        } else if ( event.detail ) {
          // this.webgl.camera.fov += event.detail * 1.0;
          aDelete -= event.detail * 1.0;
        }
        if(aDelete > 30) {
          if(this.levelindex < this.nblevels-1) {
            this.levelindex ++;
            this.webgl.levelUp(0.5);
          }
          aDelete = 0;
        } else if (aDelete < -30) {
          if(this.levelindex > 0) {
            this.levelindex --;
            this.webgl.levelDown(0.5);
          }
          aDelete = 0;
        }
      }
    },

    onKeyDown( event ) {
      // if(this.webgl.isShowingInfo) {
      //   this.popDown();
      // } else {
      //   switch(event.keyCode) {
      //     case 38:
      //       this.webgl.levelUp(0.5);
      //       break;
      //     case 40:
      //       this.webgl.levelDown(0.5);
      //       break;
      //   }
      // }
    },

    raycast() {
      raycaster.setFromCamera( mouse, this.webgl.camera );
      let intersects = raycaster.intersectObjects( this.webgl.scene.children, true );
      // console.log(this.webgl.scene.children, intersects);
      for ( let i = 0; i < intersects.length; i++ ) {
        if (intersects[ i ].object.name === 'Cube') {
          // intersects[ i ].object.material.color.set( 0xff0000 );
          // console.log("click detected", intersects[i].object.name);
        }
      }
    }
  },

  components: {
    'xp-progress-component': XpProgressComponent,
    'pop-up-component': PopUpComponent,
    'pop-up-confirmation': PopUpConfirmationComponent,
    'loader-progress-component': LoaderProgressComponent,
    'loader-interaction-component': LoaderInteractionComponent,
    'level-indicator-component': LevelIndicatorComponent,
    'footer-component': FooterComponent
  }
});
