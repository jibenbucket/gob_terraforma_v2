'use strict';

import './styles.scss';
import TweenMax from 'gsap';
import AboutCardComponent from 'components/AboutCard';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      _hidden: null
    };
  },

  created() {
    this.bind();
  },

  ready() {
    this.aboutPopup = document.getElementById('about-page-container');
    this.backButton = document.getElementById('btn-hide-popup');
    this.addEventListeners();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
     * Binding & Events
     */

    bind() {
    },

    addEventListeners() {
      this.backButton.addEventListener("click",this.hidePopup);
    },

    removeEventListeners() {
    },

    hidePopup() {
      TweenMax.to(this.aboutPopup,0.5, { opacity:0, onComplete:()=>{
        this.aboutPopup.style.display = "none";
      }});
    }
  },

  transitions: {},

  components: {
    'about-card-component': AboutCardComponent
  }
});
