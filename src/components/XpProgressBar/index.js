'use strict';

import './styles.scss';
import { clamp, normalize } from 'utils/Math';

export default Vue.extend({

  template: require('./template.html'),

  data() {
    return {
      val: 0
    };
  },

  props: {
    progress: Number,
    max: Number,
    step: Number
  },

  watch: {
    'progress': 'watchProgress'
  },

  created() {
    this.bind();
  },

  ready() {
    this.progressContainer = this.$els.xpProgressContainer;
    this.progressBarContainer = this.$els.progressBarContainer;
    this.createSticks();
  },

  methods: {

    /*
     * Binding & Events
     */

    bind() {
      // custom binding
      // this.watchProgress == ::this.watchProgress;
    },

    watchProgress( val ) {
      TweenMax.to(this.progressBarEl, 0.5,{height: val*10*(this.height/this.nbSticks)});
      // this.updateTl(val);
    },

    createSticks() {
        this.nbSticks = this.max*10;
        this.height = window.innerHeight*6/10;
        // TweenMax.set(this.progressContainer, {height: this.height})
        this.sticks = [];
        for(let i = 0; i < this.nbSticks+1; i++){
          let progressStickEl = document.createElement("span");
          if(i%10==0)
            progressStickEl.className = 'progress-long-stick';
          else
            progressStickEl.className = 'progress-short-stick';

            TweenMax.set(progressStickEl, { y: (i*(this.height/this.nbSticks)) });

          this.progressBarContainer.appendChild(progressStickEl);
          this.sticks.push(progressStickEl);
        }
        // Progress bar span
        this.progressBarEl = document.createElement("span");
        this.progressBarEl.id = "progress-bar";
        TweenMax.set(this.progressBarEl, { bottom: this.height - this.max*10*(this.height/this.nbSticks)});
        TweenMax.set(this.progressBarEl, { y: this.height});
        this.progressBarContainer.appendChild(this.progressBarEl);
        // Step stick
        let stepStickEl = document.createElement("span");
        stepStickEl.id = "step-bar";
        TweenMax.set(stepStickEl,{ y: (this.max-this.step)*10*(this.height/this.nbSticks)});
        this.progressBarContainer.appendChild(stepStickEl);

    }
  },

  transitions: {},

  components: {}
});
