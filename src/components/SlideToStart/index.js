'use strict';

import './styles.scss';
import TweenMax from 'gsap';
import Router from 'core/Router';
import States from 'core/States';
import { clamp, normalize } from 'utils/Math';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      _hidden: null
    };
  },

  props: {
    progress: Number,
    onDragProgress: Function
  },

  created() {
    this.isUserInteracting = false;
    this.bind();
  },

  ready() {
    this.sliderCont = this.$els.sliderContainer;
    this.sliderBtn = this.sliderCont.getElementsByClassName('slide-btn')[0];
    this.initialBtnLeft = this.sliderBtn.getBoundingClientRect().left;
    this.sliderSize = this.sliderCont.getBoundingClientRect().width;
    this.dragText = this.$els.dragText;

    this.addEventListeners();
    this.generateTimelines();
  },

  beforeDestroy() {
    this.removeEventListeners();
  },

  methods: {

    /*
     * Binding & Events
     */

    bind() {
    },

    addEventListeners() {
      document.addEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.addEventListener( 'mouseup', this.onDocumentMouseUp, false );
      this.sliderBtn.addEventListener( 'mousedown', this.onDocumentMouseDown, false );
    },

    removeEventListeners() {
      document.removeEventListener( 'mousemove', this.onDocumentMouseMove, false );
      document.removeEventListener( 'mouseup', this.onDocumentMouseUp, false );
    },

    generateTimelines() {

      // this.tlProgressVal = 0;
      // this.tl = new TimelineMax( { paused: true });
      // this.tlProgress = new TimelineMax( { paused: true, repeat: -1 });
      this.tlEndDrag = new TimelineMax( { paused: true });
      this.tlStartDrag = new TimelineMax( { paused: true });

      // for (let i = 0; i < this.points.length; i++) {
      //   this.tl.to( this.points[i], 0.02, { opacity: 1 } );
      //   this.tl.to( this.points[i], 0.02, { opacity: 0 } );
      //   this.tl.to( this.points[i], 0.02, { opacity: 1 } );
      // }
      //
      // this.tlProgress.to(this, 3, {
      //   tlProgressVal: 1, ease: Expo.easeOut, onUpdate: ()=>{
      //     this.tl.progress( this.tlProgressVal );
      //   }
      // });
      this.tlEndDrag.add(TweenMax.to(this.sliderBtn, 0.5, { scale: 1.3, ease: Expo.easeOut }));
      this.tlEndDrag.add(TweenMax.to(this.sliderBtn, 0.5, {
        scale: 1,
        ease: Elastic.easeOut,
        onComplete: () =>{
          this.onSliderComplete();
        }
       }));
       this.tlStartDrag.add(TweenMax.to(this.sliderBtn, 0.2, { scale: 1.2, ease: Expo.easeOut }));
       this.tlStartDrag.add(TweenMax.to(this.sliderBtn, 0.5, { scale: 1, ease: Elastic.easeOut }));
    },

    onSliderComplete() {
      if(!localStorage.oxyrate){
        localStorage.totalMoney = 0;
        localStorage.naturalizationRate = 0;
        localStorage.oxyrate = Math.trunc(Math.random()*10)/10;
        localStorage.hydrorate = Math.trunc(Math.random()*10)/100;
        localStorage.terrarate = Math.trunc(Math.random()*100)/100;
        console.log("Hello new there !");
      } else {
        console.log("welcome back, you have " + (1-localStorage.naturalizationRate)*100 + " left !");
        if(localStorage.getItem("pesticidesfinished") && States)
          States.pesticidesfinished = JSON.parse(localStorage.getItem("pesticidesfinished"));
        if(localStorage.getItem("nuclearfinished") && States)
          States.nuclearfinished = JSON.parse(localStorage.getItem("nuclearfinished"));
      }

      Router.go({ path: '/intro' });
    },

    onDocumentMouseDown( event ) {
      event.preventDefault();
      this.isUserInteracting = true;
      this.tlStartDrag.restart();
    },
    onDocumentMouseMove( event ) {
      if ( this.isUserInteracting ) {
        this.currentBtnLeft = normalize(this.initialBtnLeft, this.initialBtnLeft + this.sliderSize, this.sliderBtn.getBoundingClientRect().left);
        this.mousePosX = clamp( 0, 1, normalize(this.initialBtnLeft, this.initialBtnLeft + this.sliderSize , event.clientX));
        if (this.currentBtnLeft > 0.87) {
          this.tlEndDrag.play();
        } else {
          this.onDragProgress( clamp( 0, 1, this.currentBtnLeft ));
          TweenMax.set(this.dragText, { opacity:  (0.8 - this.currentBtnLeft)});
          TweenMax.set(this.sliderBtn, { x: (this.mousePosX * this.sliderSize) });
        }
      }
    },
    onDocumentMouseUp( event ) {
      event.preventDefault();
      this.isUserInteracting = false;
      this.currentBtnLeft = clamp( 0, 1, normalize(this.initialBtnLeft, this.initialBtnLeft + this.sliderSize, this.sliderBtn.getBoundingClientRect().left));
      if (this.currentBtnLeft > 0.87) {
        this.tlEndDrag.play();
      } else {
        TweenMax.to(this.sliderBtn, 1, {
          x: "0px",
          onUpdate: () => {
            this.currentBtnLeft = clamp( 0, 1, normalize(this.initialBtnLeft, this.initialBtnLeft + this.sliderSize, this.sliderBtn.getBoundingClientRect().left));
            this.onDragProgress( this.currentBtnLeft );
            TweenMax.set(this.dragText, { opacity:  (0.8 - this.currentBtnLeft)});
          },
          ease: Expo.easeOut
        });
      }
    }
  },

  transitions: {},

  components: {}
});
