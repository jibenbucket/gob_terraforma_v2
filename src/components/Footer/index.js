'use strict';

import './styles.scss';
import SoundManager from 'core/SoundManager';
import Emitter from 'core/Emitter';
import Router from 'core/Router';
import TweenMax from 'gsap';
import AboutComponent from 'components/About'

export default Vue.extend({

  template: require('./template.html'),

  data() {
    return {
      isMuted: false
    };
  },

  created() {
    this.bind();
  },

  ready() {
    this.soundManager = SoundManager.getInstance();
    this.sharePopup = document.getElementById('share-popup');
    this.helpPopup = document.getElementById('loader-container');
    this.aboutPopup = document.getElementById('about-page-container');
    this.aboutDisplayed = false;
    this.addEventListeners();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
     * Binding & Events
     */

    bind() {
    },

    addEventListeners() {
      this.muteButton = document.getElementById("sound_control");
      this.muteButton.addEventListener("click",this.pressedMute);
      this.shareButton = document.getElementsByClassName("share")[0];
      this.shareButton.addEventListener("click",this.pressedShare);
      this.aboutButton = document.getElementsByClassName("credits")[0];
      this.aboutButton.addEventListener("click",this.pressedAbout);
      this.helpButton = document.getElementsByClassName("help")[0];
      this.helpButton.style.opacity = 0.5;
      if(this.helpPopup) {
        this.helpButton.addEventListener("click",this.pressedHelp);
        this.startButton = document.getElementById('progress-start');
        this.startButton.addEventListener('click', this.onStartButton, false);
      }
      this.isFullScreen = false;
      this.fullScreenEl = document.getElementsByClassName("fullscreen-control")[0];
      this.fullScreenEl.addEventListener("click", this.pressedFullScreen);
      document.body.addEventListener("keyup", this.onKeyUp);
    },

    removeEventListeners() {
      document.body.removeEventListener("keyup", this.onKeyUp, false);
      this.muteButton.removeEventListener("click",this.pressedMute, false);
      this.shareButton.removeEventListener("click",this.pressedShare, false);
      this.aboutButton.removeEventListener("click",this.pressedAbout, false);
      this.helpButton.removeEventListener("click",this.pressedHelp, false);
      this.fullScreenEl.removeEventListener("click", this.pressedFullScreen, false);

    },

    onKeyUp(e) {
      if(e.key == "A" || e.key == "a") {
        this.pressedAbout();
      }
      else if(e.key == "S" || e.key == "s") {
        this.pressedShare();
      }
      else if(e.key == "H" || e.key == "h") {
        this.pressedHelp();
      }
    },

    pressedShare() {
      this.copyTextToClipboard();
      this.sharePopup.style.display = "block";
      TweenMax.to(this.sharePopup,0.5, { opacity:1, onComplete:()=> {
          TweenMax.to(this.sharePopup,0.5, { opacity:0, delay:1.5, onComplete:()=> {
            this.sharePopup.style.display = "none";
          }});
      }});
    },

    pressedAbout() {
      if(this.aboutPopup.style.display == "block") {
        TweenMax.to(this.aboutPopup,0.5, { opacity:0, onComplete:()=>{
          this.aboutPopup.style.display = "none";
        }});
      } else {
        this.aboutPopup.style.display = "block";
        this.animateCards();
        TweenMax.to(this.aboutPopup,0.5, { opacity:1});
      }
    },

    onStartButton () {
      if(this.helpReady) {
        TweenMax.to(this.helpPopup,0.5, { opacity:0, onComplete:()=> {
          this.helpPopup.style.display = "none";
        }});
      } else {
        this.helpReady = true;
        this.helpButton.style.opacity = 1;
        TweenMax.to(document.getElementById('loader-bar'), 0.2, {opacity:0});
        TweenMax.to(document.getElementById('progress-text'), 0.2, {opacity:0});
      }
    },
    pressedHelp() {
      if(this.helpReady) {
        this.helpPopup.style.display = "block";
        TweenMax.to(this.helpPopup,0.5, { opacity:1});
      }
    },

    pressedMute() {
      this.soundManager.onOffSound();
      let elsBar = document.getElementsByClassName('bar');
      if (this.isMuted){
        for (let i = 0; i < elsBar.length ; i++) {
          elsBar[i].classList.remove("muted");
          Emitter.emit('DEMUTE')
        }
      } else {
        for (let i = 0; i < elsBar.length ; i++) {
         elsBar[i].classList.add("muted");
         Emitter.emit('MUTE')
       }
      }
      this.isMuted = !this.isMuted;
    },

    animateCards() {
      let cards = document.getElementsByClassName("about-card");
      for(let i=0; i< cards.length; i++) {
        TweenMax.set(cards[i],{height:"50%"});
        // let subs = cards[i].children;
        // console.log(subs);
        // console.log(subs.length);
        // for(let i = 0; i < subs.length; i++) {
        //     subs[i].style.display = 'none';
        // }
        TweenMax.to(cards[i], 0.5, {height:"100%", delay:0.5+0.05*i, onComplete:()=>{
          // for(let i = 0; i < subs.length; i++) {
          //   subs[i].style.display = 'block';
          // }
        }});
      }
    },

    pressedFullScreen() {
      if(this.isFullScreen) {
        this.goOutFullscreen();
        this.isFullScreen = false;
      } else {
        this.goInFullscreen();
        this.isFullScreen = true;
      }
    },

    goInFullscreen() {
      let element = document.body;
      // let threeElement = document.getElementById("three-container");
      if(element.requestFullscreen){
    	   // threeElement.requestFullscreen();
         element.requestFullscreen();
      }
    	else if(element.mozRequestFullScreen) {
    	   // threeElement.mozRequestFullScreen();
         element.mozRequestFullScreen();
       }
    	else if(element.webkitRequestFullscreen){
    	   // threeElement.webkitRequestFullscreen();
         element.webkitRequestFullscreen();
       }
    	else if(element.msRequestFullscreen){
    	   // threeElement.msRequestFullscreen();
         element.msRequestFullscreen();
       }
    },

    goOutFullscreen() {
    	if(document.exitFullscreen)
    	 document.exitFullscreen();
    	else if(document.mozCancelFullScreen)
    	 document.mozCancelFullScreen();
    	else if(document.webkitExitFullscreen)
    	 document.webkitExitFullscreen();
    	else if(document.msExitFullscreen)
    	 document.msExitFullscreen();
    },

    copyTextToClipboard() {
      let text = "http://terraforma.xyz";
      let textArea = document.createElement("textarea");

      textArea.style.position = 'fixed';
      textArea.style.top = 0;
      textArea.style.left = 0;
      textArea.style.width = '2em';
      textArea.style.height = '2em';
      textArea.style.padding = 0;
      textArea.style.border = 'none';
      textArea.style.outline = 'none';
      textArea.style.boxShadow = 'none';
      textArea.style.background = 'transparent';

      textArea.value = text;

      document.body.appendChild(textArea);

      textArea.select();

      try {
        let successful = document.execCommand('copy');
        let msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copied ' + msg);
      } catch (err) {
        console.log('Oops, unable to copy');
      }

      document.body.removeChild(textArea);
    }
  },

  transitions: {},

  components: {
    "about-component": AboutComponent
  }
});
