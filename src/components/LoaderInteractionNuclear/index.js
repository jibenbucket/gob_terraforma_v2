'use strict';

import './styles.scss';
import TweenMax from 'gsap';
import Router from 'core/Router';
import SoundManager from 'core/SoundManager';
import { clamp, normalize } from 'utils/Math';

export default Vue.extend({

  template: require('./template.html'),

  created() {
  },

  ready() {
    this.handSvg = this.$els.handSvg;
    this.animate();
    SoundManager.demute();
  },

  beforeDestroy() {
    this.handSvgTl.kill();
  },

  methods: {
    animate() {
      this.handSvgTl = new TimelineMax( { paused: false });
      this.handSvgTl.to(this.handSvg,0.4,{marginLeft:0})
      .to(this.handSvg,0.8,{marginLeft:50, onComplete:()=>{this.animate();}});
    }
  },

  transitions: {},

  components: {}
});
