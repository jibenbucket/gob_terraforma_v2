'use strict';

import './styles.scss';

export default Vue.extend({

  template: require('./template.html'),

  props: {
    levelindex: Number,
    nblevels: Number
  },

  watch: {
    'levelindex': 'watchIndex'
  },

  ready() {
    this.levelElsContainer = this.$els.levelIndicatorContainer;

    this.levelEls = [];
    this.levelEls[0] = document.createElement('div');
    this.levelEls[0].id = 'level'+0;
    this.levelEls[0].innerHTML = 'Lvl '+1;
    this.levelEls[0].classList.add("active");
    this.levelElsContainer.appendChild(this.levelEls[0]);
    for (let i = 1; i < this.nblevels; i++) {
      this.levelEls[i] = document.createElement('div');
      this.levelEls[i].id = 'level'+(i);
      this.levelEls[i].innerHTML = 'Lvl '+(i+1);
      this.levelElsContainer.appendChild(this.levelEls[i]);
    }

    this.previousLevelIndex = 0;
  },

  beforeDestroy() {
  },

  methods: {
      watchIndex(val) {
        this.levelEls[this.previousLevelIndex].classList = "";
        this.previousLevelIndex = this.levelindex;
        this.levelindex = val;
        this.levelEls[val].classList.add("active");
      }
  },

  transitions: {},

  components: {}
});
