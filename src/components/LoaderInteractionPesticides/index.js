'use strict';

import './styles.scss';
import TweenMax from 'gsap';
import Router from 'core/Router';
import SoundManager from 'core/SoundManager';
import { clamp, normalize } from 'utils/Math';

export default Vue.extend({

  template: require('./template.html'),

  created() {
  },

  ready() {
    this.handSvg = this.$els.handSvg;
    this.img = this.$els.loaderImg;
    this.animate();
    SoundManager.demute();
  },

  beforeDestroy() {
  },

  methods: {
    animate() {
      let handSvgTl = new TimelineMax( { paused: false });
      handSvgTl.to(this.handSvg,0.4,{marginLeft:30,marginBottom:60,
        onUpdate:()=>{TweenMax.set(this.img,{opacity: 1-(parseInt(this.handSvg.style.marginLeft)+20)/50});}})
      .to(this.handSvg,0.8,{marginLeft:-20,marginBottom:110,
        onUpdate:()=>{TweenMax.set(this.img,{opacity: 1-(parseInt(this.handSvg.style.marginLeft)+20)/50});},
        onComplete:()=>{this.animate();}});
    }
  },

  transitions: {},

  components: {}
});
