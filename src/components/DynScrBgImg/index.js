'use strict';

import './styles.scss';
import { clamp, normalize } from 'utils/Math';
import imageLoader from 'utils/ImageLoader';

export default Vue.extend({

  template: require('./template.html'),

  data() {
    return {
      val: 0
    };
  },

  props: {
    progress: Number
  },

  watch: {
    'progress': 'watchProgress'
  },

  created() {
    this.bind();
    this.imageIndex = 1;
    this.images = new imageLoader({
      imagesNb: 30,
      path: "/images/landing_page/img_background/zap_0"
    });
  },

  ready() {
    this.bgImgContainer = this.$els.dynBgImgContainer;
    this.context = this.bgImgContainer.getContext("2d");
    this.addEventListeners();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
     * Binding & Events
     */

    bind() {
    },

    addEventListeners() {
    },

    removeEventListeners() {
    },

    updateBgImg(val) {
      let i = Math.trunc(val * 100 / 3)+1;
      if (i != this.imageIndex) {
        this.context.drawImage(this.images.imgArray[i], 0,0, this.bgImgContainer.width, this.bgImgContainer.height);
        this.imageIndex = i;

      }
    },

    watchProgress( val ) {
      this.updateBgImg(val);
    }

  },

  transitions: {},

  components: {}
});
