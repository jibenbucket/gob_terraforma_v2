'use strict';

import './styles.scss';
import scrambleText from 'utils/ScrambleTextPlugin';
import { clamp, normalize } from 'utils/Math';

export default Vue.extend({

  template: require('./template.html'),

  props: {
    loaderprogress: Number
  },

  watch: {
    'loaderprogress': 'watchProgress'
  },

  created() {
    this.bind();
  },

  ready() {
    this.progressBtn = document.getElementById('progress-start');
    this.loaderBar = this.$els.loaderBar;
    this.progressText = this.$els.progressText;
    this.progressStartSpan = this.$els.progressStartSpan;
    this.hoverDone = false;
    this.addEventListeners();
    this.createSticks();
    setInterval(()=> {
          TweenMax.to(this.progressStartSpan, 1.5, { scrambleText: { text: "GOT IT", chars: "abcdefghijklmnopqrstuvwxyz-°(){}&@_", speed: 0.5}});

    }, 4000);

  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
     * Binding & Events
     */

    bind() {
      // custom binding
      // this.watchProgress == ::this.watchProgress;
    },

    addEventListeners() {
    },

    removeEventListeners() {
    },

    watchProgress( val ) {
      TweenMax.to(this.currentProgressBarEl, 0.2,{width: val*this.width});
      this.progressText.innerHTML = Math.trunc(val*100) + "%";
    },

    createSticks() {
        this.width = window.innerWidth*4/10;
        this.nbSticks = this.width/14;
        // TweenMax.set(this.loaderBar, {width: this.width});
        // this.sticks = [];
        for(let i = 0; i < this.nbSticks; i++){
          let progressBarEl = document.createElement("span");
          TweenMax.set(progressBarEl, { left: (i*(this.width/this.nbSticks)) });
          this.loaderBar.appendChild(progressBarEl);
          // this.sticks.push(progressBarEl);
        }
        // Progress bar span
        this.currentProgressBarEl = document.createElement("span");
        this.currentProgressBarEl.id = "current-load-progress-bar";
        // TweenMax.set(this.currentPgressBarEl,{ left: this.height - this.max*10*(this.height/this.nbSticks)});
        // TweenMax.set(this.currentPgressBarEl, { y: this.height});
        this.loaderBar.appendChild(this.currentProgressBarEl);


    },

    isHover(e) {
      return (e.parentElement.querySelector(':hover') === e);
    }

  },

  transitions: {},

  components: {}
});
