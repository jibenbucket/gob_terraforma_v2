'use strict';

import './styles.scss';

import Emitter from 'core/Emitter';
import {
  HIDE_INFOS_POP
} from 'config/messages';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      _hidden: null
    };
  },

  props: {
    date: String,
    title: String,
    location: String,
    lat: Number,
    lon: Number,
    image: String,
    text: String,
    id: Number
  },

  created() {
    this.bind();
  },

  ready() {
    this.container = this.$els.popUpContainer;

    this.addEventListeners();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
     * Binding & Events
     */

    bind() {
    },

    addEventListeners() {
    },

    removeEventListeners() {
    },
    closeInfoPopUp() {
      Emitter.emit(HIDE_INFOS_POP);
    }

  },

  transitions: {},

  components: {}
});
