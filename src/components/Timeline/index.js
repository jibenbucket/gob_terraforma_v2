'use strict';

import './styles.scss';
import { clamp, normalize } from 'utils/Math';

export default Vue.extend({

  template: require('./template.html'),

  props: {
    progress: Number
  },

  watch: {
    'progress': 'watchProgress'
  },

  ready() {
    this.tlContainer = this.$els.timelineContainer;
    this.tlSticks = [];
    this.tlPoints = [];
    this.yearNb = this.$els.yearNb;
    this.currentYearSticks = this.$els.currentYearSticks;
    this.createTlStick();
  },

  methods: {
    createTlStick(){
      let yearNbr = (window.innerWidth+100)/10;
      for(let i = 0; i < yearNbr; i++){
          let yearStickEl = document.createElement("span");
          let yearPointEl = document.createElement("span");
          yearStickEl.className = 'year-stick';
          yearPointEl.className = 'year-point';
          TweenMax.set(yearStickEl, { x: (i*10 + 5) });
          TweenMax.set(yearPointEl, { x: (i*10 + 5) });
          this.tlContainer.appendChild(yearStickEl);
          this.tlContainer.appendChild(yearPointEl);
          this.tlSticks.push(yearStickEl);
          this.tlPoints.push(yearPointEl);
      }
      this.tlSticksNb = this.tlSticks.length;
      TweenMax.set(this.tlSticks[0], { scale: 1.3 });
      TweenMax.set(this.tlPoints[0], { opacity: 1 });
    },

    watchProgress( val ) {
      this.updateTl(val);
    },

    updateTl(val) {
      let normVal = Math.trunc(val * this.tlSticksNb);
      TweenMax.set(this.yearNb, { opacity: (1-val)*0.9 });
      TweenMax.set(this.currentYearSticks, { x: normVal*10 + 5 });
      TweenMax.set(this.yearNb,{ x: (normVal*10 + 3) });
      if (normVal < 63) {
        this.yearNb.innerHTML = Math.trunc(2000+normVal);
      } else if (normVal < 80) {
        this.yearNb.innerHTML = Math.trunc(200+normVal/10)+"?";
      } else if (normVal < 90) {
        this.yearNb.innerHTML = Math.trunc(20+normVal/100)+"??";
      } else {
        this.yearNb.innerHTML = 2+"???";
      }
    }
  }
});
