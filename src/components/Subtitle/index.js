'use strict';

import './styles.scss';

import {
  SUBTITLE_SHOW,
  SUBTITLE_HIDE
} from 'config/messages';

import TweenMax from 'gsap';

import Emitter from 'core/Emitter';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      text: '',
      showed: false
    };
  },

  created() {
    this.bind();
  },

  ready() {
    Emitter.on(SUBTITLE_SHOW, ({ text }) => {
      this.show(text);
    });
    Emitter.on(SUBTITLE_HIDE, () => {
      this.hide();
    });
    this.addEventListeners();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    show(text) {
      this.showed = true;
      this.text = text;
    },
    hide() {
      this.showed = false;
    },

    /*
     * Binding & Events
     */

    bind() {
    },

    addEventListeners() {
    },

    removeEventListeners() {
    }

  },

  transitions: {},

  components: {}
});
