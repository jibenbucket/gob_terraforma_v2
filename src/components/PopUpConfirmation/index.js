'use strict';

import './styles.scss';
import TweenMax from 'gsap';
import States from 'core/States';
import Router from 'core/Router';
import Emitter from 'core/Emitter';
import {
  HIDE_POPUP_CHOICES
} from 'config/messages';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      _hidden: null
    };
  },

  props: {
    textPop: String,
    accomplishment: Number,
    popType: String
  },

  created() {
    this.bind();
  },

  ready() {
    this.popUpBg = this.$els.popUpBg;
    this.popUpContainer = this.$els.popUpContainer;
    this.btnCancelTxt = this.$els.btnCancelTxt;
    this.btnConfirmTxt = this.$els.btnConfirmTxt;
    this.generateTl();

    this.addEventListeners();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {
    /*
     * Binding & Events
     */

   generateTl() {
     this.tlDismissPopUp = new TimelineMax( { paused: true });
     this.tlDismissPopUp
     .fromTo([this.popUpBg, this.popUpContainer], 0.5, { opacity: 0.8 } , { opacity : 0 }, Expo.easeIn, "-=0.1")
     .to(this.popUpBg, 0.1, { display: "none", ease: Expo.easeIn });
    //  .fromTo(this.popUpContainer, 0.7, { scale: 1, x: "-50%", y:"-50%" }, { scale: 0, x: "-50%", y:"-50%", ease: Expo.easeIn}, "-=1");
   },

    dismissPopup() {
      this.tlDismissPopUp.restart();
      Emitter.emit(HIDE_POPUP_CHOICES);
    },
    confirmPopUp() {
      if (this.popType == 'zeroStep' ){
        Router.go({
          name: 'panorama'
        });
      }
      else {
        Router.go({
          name: 'buy-resources'
        });
      }

    },
    bind() {
    },

    addEventListeners() {
    },

    removeEventListeners() {
    }

  },

  transitions: {},

  components: {}
});
