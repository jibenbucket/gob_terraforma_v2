'use strict';

import './styles.scss';

export default Vue.extend({

  template: require('./template.html'),

  data() {

    return {
      _hidden: null
    };
  },

  props :{
    type: String,
    name: String,
    imagesrc:String,
    twitterid: String
  },

  created() {
    this.bind();
  },

  ready() {

    this.addEventListeners();
  },

  beforeDestroy() {

    this.removeEventListeners();
  },

  methods: {

    /*
     * Binding & Events
     */

    bind() {
    },

    addEventListeners() {
    },

    removeEventListeners() {
    }

  },

  transitions: {},

  components: {}
});
