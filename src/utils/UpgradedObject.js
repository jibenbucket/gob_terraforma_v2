export function upgradeObject( obj ) {
  obj.isFound = false;
  obj.counter = 0;
}

export function upgradeObjectWithFoundAndCounter(obj, isFound, counter) {
  obj.isFound = isFound;
  obj.counter = counter;
}

export function upgradeObjectWithMultiplier( obj, multiplier ) {
  obj.isFound = false;
  obj.counter = 0;
  obj.multiplier = multiplier;
}

export function upgradeObjectWithMultiplierAndMate( obj, multiplier, material ) {
  obj.isFound = false;
  obj.counter = 0;
  obj.multiplier = multiplier;
  obj.mate = material;
}
