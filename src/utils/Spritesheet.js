
import now from 'performance-now';
import sortBy from 'lodash.sortby';

/**
* @class Spritesheet
*/
export default class Spritesheet {

  /**
  * constructor method
  * @param {array} datas - Collection of texture packer json data
  */
  constructor(datas, {
    width = null,
    height = null,
    path = '/',
    fps = 25,
    startAt = 0,
    autoStyle = true,
    // TODO Support center pivot point
    center = false,
    retina = false,
    container = null,
    canvas = null,
    loop = true,
    autoplay = false,
    onLoad = () => {},
    onComplete = () => {}
  } = {}) {

    this.options = {
      width,
      height,
      path,
      fps,
      startAt,
      autoStyle,
      center,
      retina,
      container,
      canvas,
      loop,
      onLoad,
      onComplete
    };

    if (!canvas) {
      this.canvas = document.createElement('canvas');
    }

    this.stopped = false;

    this.finished = false;

    this.autoplay = autoplay;

    this.datas = datas;

    this.loaded = false;

    this.sprites = [];

    this.spritesLength = 0;

    this.imagesLoaded = 0;

    this.startAt = this.options.startAt;

    this.autoStyle = autoStyle;

    this.frame = this.startAt || 0;

    this.devicePixelRatio = window.devicePixelRatio || 1;

    this.lastCall = null;

    this.dismissUpdate = false;

    this.frameWidth = 0;

    this.frameHeight = 0;

    if (
      !this.options.width || !this.options.height ||
      typeof this.options.width === 'undefined' || typeof this.options.width === 'undefined') {

      throw new Error('Width and Height must be defined');

    } else {
      this.bind();
      this.initialize();

    }

  }

    /**
    * Bind method - Bind context of the class
    */
  bind() {
    this.onImageLoaded = this.onImageLoaded.bind(this);
    this.render = this.render.bind(this);
  }


    /**
    * initialize method - Initialize spritsheet component
    */
  initialize() {

    let i = this.datas.length;
    while (i--) {

      const json = this.datas[i];

      const image = new Image();

      let j = json.frames.length;

      while (j--) {

        const current = json.frames[j];

        this.frameWidth = current.sourceSize.w;

        this.frameHeight = current.sourceSize.h;

        const data = {
          name: parseInt(current.filename.replace(/\D+/g, ''), 10),
          sx: current.frame.x,
          sy: current.frame.y,
          sw: current.frame.w,
          sh: current.frame.h,
          dx: current.spriteSourceSize.x,
          dy: current.spriteSourceSize.y,
          dw: current.spriteSourceSize.w,
          dh: current.spriteSourceSize.h,
          image,
          rotated: current.rotated,
          pivot: current.pivot
        };
        // console.log('data', data.name);

        this.sprites.push(data);
      }


      image.onload = this.onImageLoaded;

      image.src = this.options.path + json.meta.image;
    }

    this.sprites = sortBy(this.sprites, 'name');

    this.spritesLength = this.sprites.length;

  }

    /**
    * onImageLoaded method - Each image load callback
    */
  onImageLoaded() {

    this.imagesLoaded++;

    if (this.imagesLoaded === this.datas.length) {

      this.onComplete();
    }

  }

    /**
    * onComplete method - Trigger when all image are loaded
    */
  onComplete() {

    const canvas = this.options.canvas ? this.options.canvas : this.canvas;

    const context = canvas.getContext('2d');

    if (this.options.container && !this.options.canvas) {

      this.options.container.appendChild(canvas);

    }

    const retina = this.options.retina ? 2 : 1;

    canvas.width = this.options.width * retina;

    canvas.height = this.options.height * retina;

    if (this.autoStyle) {

      canvas.style.width = `${this.options.width}px`;

      canvas.style.height = `${this.options.height}px`;

    }

    this.context = context;

    this.canvas = canvas;

    this.loaded = true;

    this.options.onLoad();

    if (this.autoplay) {

      this._raf = requestAnimationFrame(this.render);

    }

  }

    /**
    * play method - Launch sprite animation
    */
  play() {

    if (this.loaded) {

      this._raf = requestAnimationFrame(this.render);

    } else {

      this.autoplay = true;
    }

  }

    /**
    * update method - Update call by raf
    */
  update() {

    if (this.lastCall === null) {

      this.start();
    }

    const n = now();

    const dt = (n - this.lastCall).toFixed(3);

    if (dt > 1000 / this.options.fps) {

      this.lastCall = n;

      if (this.frame < this.spritesLength - 1) {

        this.frame++;

      } else if (this.options.loop) {

        this.frame = 0;

      } else {

        this.finished = true;
        this.options.onComplete();

      }

    }

  }

    /**
    * render method
    * @param {int} ts - ts
    * @param {boolean} dismissUpdate - if true, do not call update
    */
  render(ts) {

    if (!this.loaded || this.stopped) return;

    const context = this.context;

    if (this.dismissUpdate) {

      this._raf = requestAnimationFrame(this.render);

      this.update();

      this.dismissUpdate = false;

    }


    const sprite = this.sprites[this.frame];

    const ratio = this.options.retina ? 2 : 1;

    context.clearRect(0, 0, this.canvas.width, this.canvas.height);

    context.save();

    // sprite.rotated = true
    if (sprite.rotated) {

      context.translate(this.canvas.width / 2, this.canvas.height / 2);

      context.rotate(-Math.PI / 2);

      context.translate(-this.canvas.height / 2, -this.canvas.width / 2);

      const height = this.canvas.height;


      // Axis are now reverse due to context rotation
      // origin on bottom left corner
      // x -> vec2(0,-1) y -> vec2(1,0)

      const newDy = height - sprite.dh - sprite.dy;

      context.drawImage(
        sprite.image,
        sprite.sx,
        // The X coordinate of the top left corner of the sub-rectangle of the source image
        sprite.sy,
        // The Y coordinate of the top left corner of the sub-rectangle of the source image
        sprite.sh, // new sw = sh
        sprite.sw, // new sh = sw
        newDy, // new Dy
        sprite.dx, // new dy = dx
        sprite.dh, // new dw = dh
        sprite.dw // new dh = dw
      );

    } else {

      context.drawImage(
        sprite.image,
        sprite.sx,
        sprite.sy,
        sprite.sw,
        sprite.sh,
        sprite.dx,
        sprite.dy,
        sprite.dw,
        sprite.dh
      );
    }

    context.restore();

    if (this.finished) cancelAnimationFrame(this._raf);

  }

    /**
    * start method
    */
  start() {

    this.stopped = false;

    this.frame = this.startAt;

    this.lastCall = now();

  }

    /**
    * stop method
    */
  stop() {

    this.frame = 0;

    this.lastCall = null;

    this.stopped = true;

    cancelAnimationFrame(this._raf);

  }

    /**
    * goTo method
    * @param {int} frame - frame number
    * @param {float} progress - frame between 0 and 1
    */
  goTo({ frame, progress }) {

    if (frame < this.spritesLength) {

      this.frame = frame;

    } else if (typeof progress === 'number') {

      // FIXME: clamp with a proper way
      progress = Math.max(0, Math.min(1, progress));

      this.dismissUpdate = true;

      this.frame = Math.floor(progress * (this.spritesLength - 1 ));

    } else {

      this.frame = 0;

      console.warn('Invalide frame number');
    }

    requestAnimationFrame(this.render);

  }

  }
