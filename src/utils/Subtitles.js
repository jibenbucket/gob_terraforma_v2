import {
  SUBTITLE_SHOW,
  SUBTITLE_HIDE
} from 'config/messages';

import Emitter from 'core/Emitter';

export default class Subtitle {
  constructor({ timeline, player }) {
    this.timeline = timeline;
    this.timelineLength = timeline.length;
    this.player = player;
    for (let i = 0; i < this.timelineLength; i++) {
      this.timeline[i].read = false;
      if (this.timeline[i].timecode.end < this.player.currentTime) {
        this.timeline[i].read = true;
      }
    }
    this.update = this.update.bind(this);
    this.update();
  }
  update() {
    this.raf = window.requestAnimationFrame(this.update);
    const currentTime = this.player.currentTime;
    for (let i = 0; i < this.timelineLength; i++) {
      if (currentTime >= this.timeline[i].timecode.start && this.timeline[i].read === false) {
        this.timeline[i].read = true;
        if (this.timeline[i].text !== 'undefined') {
          Emitter.emit(SUBTITLE_SHOW, {
            text: this.timeline[i].text
          });
        }
        const events = this.timeline[i].events || 0;
        for (let j = 0; j < events.length; j++) {
          if (typeof events[j] === 'string') {
            Emitter.emit(events[j]);
          } else if (typeof events[j] === 'object') {
            setTimeout(() => {
              Emitter.emit(events[j].name, events[j].params);
            }, events[j].delay * 1000 || 0);
          }
        }
      }
      if (currentTime >= this.timeline[i].timecode.end && this.timeline[i].read === true) {
        if (this.timeline[i].text !== 'undefined') {
          Emitter.emit(SUBTITLE_HIDE, 'hide');
        }
        this.timeline.splice(i, 1);
        this.timelineLength = this.timeline.length;
      }
    }
  }
  destroy() {
    if (typeof (this.raf !== 'undefined') && this.raf !== null) {
      window.cancelAnimationFrame(this.raf);
      this.raf = null;
    }
  }

}
