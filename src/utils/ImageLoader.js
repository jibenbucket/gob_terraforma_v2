
/**
* @class ImageLoader
*/
export default class ImageLoader {

  constructor({
    imagesNb = 0,
    path = null,
    onLoad = () => {},
    onComplete = () => {}
  } = {}) {

    this.options = {
      imagesNb,
      path,
      onLoad,
      onComplete
    };


    this.path = this.options.path;

    this.pathArray = [];

    this.loaded = false;

    this.onImageLoaded = 0;

    this.imgArray = [];

    this.imagesNb = this.options.imagesNb;

    this.initializePath();
    this.loadImages();


  }

  initializePath() {
    for (let i = 0; i < this.imagesNb; i++) {
      let path = this.path+i+".jpg";
      this.pathArray.push(path);
      // console.log(this.pathArray);
    }

  }

  loadImages(){
    let i = this.pathArray.length;
    while (i--) {
      const image = new Image();
      image.src = this.pathArray[i];
      this.imgArray.push(image);
    }
    this.loaded = true;
  }


}
