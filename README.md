# Terraforma 🌍🌱

##### Install dependancies (might need sudo) :
```shell
  npm i
```

##### Download the videos at 
```
https://www.dropbox.com/sh/1x3rnvbbxwumhpj/AACcuh06kyYh7crm8tNYkgzsa/session%20%238/video%20compress%C3%A9?dl=0
```
##### Put them in 
```
  static/videos
```

##### Launch the project :
```shell
  npm start
```

The project will be launched at http://localhost:3000/

#### Build for production :
```shell
  npm run build
```

#### TECHNOLOGIES

* VueJs
* ThreeJS
* EventEmitter
* GSAP - TweenMax
* Babel | ES2015
* Webpack
* ESLint
* SASS